# UM-Pilot

UM bestaat uit twee software componenten.
1. UM
2. Externe vertaalapplicatie

Beiden componenten zijn te vinden in dit project.
In de main is de documentatie voor UM en de externe vertaalapplicatie te vinden.

Deze software is nog in ontwikkeling en is bedoeld als beproeving van de architectuurprincipes en omvat de MVP functionaliteit van UM.
Deze functionaliteit is voldoende om de VUM pilots te kunnen ondersteunen.

De code is beschikbaargesteld onder de EUPL 1.2 voorwaarden
https://www.eupl.eu/1.2/nl/

# Documentatie

* [User stories in sequence diagrammen](/Technisch%20Ontwerp/userstories.md)
* [Componenten UM omgeving](/Technisch%20Ontwerp/componenten%20UM%20omgeving.md)
  * Dit document beschrijft de componenten van de UM omgeving met de belangrijkste kenmerken en configuratie files..
  * Tevens bevat dit document een lijst van API's (web services).
* [Opzetten van de omgeving in docker (lokaal/ontwikkelomgeving)](/DevOps/opzetten%20UM%20in%20Docker.md)
* [CICD en release proces](/DevOps/CICD%20en%20UM%20Releases.md)
* [Functionele userstories in hoofdlijnen](/User%20Stories/Functioneel)
  * De gedetailleerde userstories zijn uitgewerkt in de modules waar zijn betrekking op hebben
* [Logging](/User%20Stories/Logging)
  * De userstories met betrekking tot functionele (ihkv de AVG en de BIO) en technische logging zijn 'cross-cutting concerns' en hier opgenomen als algemene documentatie 

Hieronder volgen guidelines t.b.v het documenteren zelf.

# Documentatie guidelines

### Doel
Doel van dit hoofdstuk is om algemene tips t.b.v van documentie te geven.

## Index
Voeg documentatie toe aan de index boven in dit bestand, zodat deze makkelijker teruggevonden kan worden.

## Diagrammen en schema's
Om te zorgen dat de diagrammen en schema's, zoals bijvoorbeeld een sequence diagram, makkelijk aanpasbaar zijn en blijven, én bovendien toonbaar zijn in ieders browser:

Wanneer diagrammen in [Draw.io](https://www.draw.io) worden gemaakt of aangepast, kan het Draw.io bronbestand mee in het bestand worden opgeslagen in het .png of .svg bestand.  
De volgende keer dat dit bestand wordt geopend in Draw.io, wordt deze niet als plaatje geopend, maar als diagram/schema waarin de afzonderlijke object aanpasbaar zijn.

Wanneer het bestand in een ander programma wordt geopend en overschreven, verdwijnt dit voordeel.

[Draw.io] kan offline gebruikt worden, door het programma te downloaden en installeren.
