# Opzetten UM Docker omgeving in (lokale) Docker


| versie | datum         | opmerking                                    |
|--------|---------------|----------------------------------------------|
| 0.1    | augustus 2022 | Initiele opzet op basis van eerdere notities |
| 0.2    | augustus 2022 | bijwerken                                    |
| 0.3    | december 2022 | Beschrijving Keycloak apart opstarten        |
| 0.4    | januari  2023 | Bijgewerkt om het mogelijk te maken van andere Oauth/identity servers dan Keycloak gebruik te maken |
| 0.5    | maart 2023    | Configuratie per OIN: naam deelnemer/gemeente en logo                                               |

## 1 Draaien van UM applicatie op ontwikkelomgeving met behulp van Docker Containers

### 1.1 Doel

Dit document beschrijft de stappen die nodig zijn om de UM omgeving te laten draaien op Docker containers in een ontwikkelomgeving. Voor het authentiseren en authoriseren in een Identity Server nodig, in dit document wordt uitgegaan van Keycloak als Identity Server.


### 1.2  Benodigdheden

Om het UM-portaal lokaal te laten draaien zijn de volgende zaken nodig:

* Geconfigureerde Docker omgeving. Op Windows kan, bijvoorbeeld, gebruik worden gemaakt van Docker Desktop (let wel op de licenties voor dit product): https://www.docker.com/

* Git client (aanbevolen).

### 1.3 Stappen

#### 1.3.1 Aanmaken juist folder structuur

Maak een folder aan, bijvoorbeeld 'VNG', met daarin de volgende folders:
* um-compose
* keycloak
* gateway
* assets
  * logos

#### 1.3.2 Git repositories downloaden (clonen)

De bestanden uit de volgende Git repositories zijn nodig om de applicatie lokaal te laten draaien:
* [Um-compose][100]
* [Keycloak](https://gitlab.com/vng-realisatie/um-pilot/keycloak)
* [Kong API Gateway][200]

De bovenstaande repositories moeten gekloond worden in de eerder aangemaakte folders.
Hier volgt een stappenplan middels het gebruik van Sourcetree als Git client:
```
Begonnen wordt met de 'um-compose' Git repo.
* druk op clone in de Gitlab pagina van um-compose( https://gitlab.com/vng-realisatie/um-pilot/um-compose) en kopieer het pad met https
* in Sourcetree ga naar File, Clone en plak het gekopieerde pad in het veld voor 'source path' 
* vul bij 'destination path' het pad waar de source lokaal zullen worden opgeslagen. Kies het mapje 'um-compose' uit stap 1.
* klik op de 'clone' knop.

Herhaal deze stappen voor de hierboven genoemde Git repo's.
```

De folder assets met sub folder logos wordt in de volgende stap gevuld.

#### 1.3.3 Naam en logo gemeente
Om de naam en het logo van de gemeente (waartoe de op UM ingelogde gebruikers behoren) in de UM applicatie te gebruiken, moet in de assets folder een configuratie file worden geplaatst en een afbeelding met het logo van de gemeente:
* configuratie file: json file, met daarin een lijst van OINs (1 of meer) met de naam van de bijbehorende organisaties. De file moet dezelfde naam en structuur bevatten als file [310], daar deze de file aanwezig in het docker image vervangt bij het opstarten van de container.
* logo: te plaatsen in de sub folder logos. De bestandsnaam van het logo moet overeenkomen met het OIN nummer en bestands extensie png hebben: [OIN].png

#### 1.3.4 (MS Windows) Virtual Memory Areas setting Docker Desktop
Bij een Docker Desktop installatie krijgt Docker standaard te weinig capaciteit toegewezen voor Virtual Memory Areas (VMA),
 wat zich uit in plotseling stoppende containers  (Elastic) van de UM applicatie.

Voer daarom het commando volgende uit bij elke start van Docker Desktop:

Open “Command prompt”  of “Windows PowerShell" 

zodra de terminal geopend is voer het volgende command uit:
```
wsl -d docker-desktop sysctl -w vm.max_map_count=262144 
```

![foto1.png](opzetten%20UM%20in%20Docker-images/foto1.png)


#### 1.3.5 Opstarten van de Docker containers:

_Keycloak als Identity Server_  
Keycloak moet apart opgestart worden. Navigeer hiertoe in een terminal naar de [docker](https://gitlab.com/vng-realisatie/um-pilot/keycloak/-/tree/develop/docker) folder in de folder keycloak. En voer het docker-compose up commando uit. Maak hierbij gebruik van [het properties bestand .env][102], dat zich in dezelfde map bevindt:
```
docker compose -p iam-um --env-file .env up
```

De waarde van de property KEYCLOAK_FRONTEND_URL moet overeenkomen met het eerste gedeelte uit de JWT_ISSUER_URI [property][102] in het [UM-Compose project][100].

```
Indien deze waarden:
* niet met elkaar overeenstemmen of 
* een hostname bevat die, wanneer aangeroepen van binnenuit een docker container, niet gevonden kan worden
dan kunnen gebruikers mogelijk niet inloggen en/of zullen onderliggende API calls mislukken (401 Unauthorized meldingen). 

In een setting met een lokale Windows pc i.c.m. Docker Desktop zal deze 'host.docker.internal' bevatten, in een andere setting moet wellicht gekozen worden voor een andere hostname of gewoon 'localhost'. 
```

_UM_  
Open een terminal, bv PowerShell in Windows, en navigeer naar de UM-Compose map.

Voer het volgende commando uit om de UM docker containers op te starten: docker-compose up, al dan niet met een expliciete verwijzing naar een properties file

```
docker compose --env-file .env up
```

 ![foto4.png](opzetten%20UM%20in%20Docker-images/foto4.png)  

Navigeer in een internet browser naar localhost:8080 waar de applicatie UM nu te zien moet zijn.

Je kan inloggen met

| gebruiker | wachtwoord |
|-----------|------------|
| test      | test       |
| test2     | test2      |

Mocht je zelf gebruikers willen aanmaken met verschillende rollen, dat kan in [Keycloak](http://localhost:8083):
```
Klik op 'Administration console' en log in met gebruiker 'admin' en wachtwoord 'admin'

Ga naar het tabje links onderin bij Users
Je kan hier view all users doen om te zien welke user er al zijn.
Je kan ook aan de rechterkant op Add user klikken
Voor nu is het enige van belang om een username toe te kennen.

Nu ga je naar het tabje **Attributes** hier vul je bij key: oin en bij value: de oin value van de gemeenten

Daarna ga je naar **Credentials** hier geef je de nieuwe user een password

Daarna ga je naar **Role Mappings** hier kun verschillende roles toevoegen aan je user.
```

# 2 Development settings

## 2.1 werkzoekende profielen bemiddelaar

### [application properties][502]
```
server.port=${BMW_SERVER_PORT:8088}
server.address=${BMW_SERVER_ADDRESS:0.0.0.0}

logging.level.org.springframework=${BMW_LOGGING_LEVEL:DEBUG}
logging.file.path=${BMW_LOGGING_FILE_PATH:.}
logging.file.name=${BMW_LOGGING_FILE_NAME:app.log}

spring.security.oauth2.resourceserver.jwt.issuer-uri=${BMW_JWT_ISSUER_URI:http://localhost:8083/auth/realms/poc-vng-develop-realm}

spring.h2.console.enabled=${BMW_CONSOLE_ENABLED:true}
spring.datasource.url=${BMW_DATASOURCE_URL:jdbc:h2:./werkzoekende-bemiddelaar;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE}
spring.datasource.driverClassName=${BMW_DATASOURCE_DRIVER:org.h2.Driver}
spring.datasource.username=${BMW_DATASOURCE_USERNAME:sa}
spring.datasource.password=${BMW_DATASOURCE_PASSWORD:}
spring.jpa.database-platform=${BMW_DATABASE_PLATFORM:org.hibernate.dialect.H2Dialect}
spring.jpa.hibernate.ddl-auto=${BMW_HIBERNATE_DDL:update}

cap.days-to-expiry=${BMW_DAYS_TO_EXPIRE:7}
cap.max-times-accessible=${BMW_MAX_DETAIL_PROFILES:10}
cap.callback-url=${BMW_CALLBACK_URL_BEMIDDELAAR:http://localhost:8081/aanvraagwerkzoekende/callback}
cap.vum-url-matches=${BMW_VUM_URL_MATCHES:http://localhost:8100/api/v1/werkzoekendeProfielen/matches}
cap.vum-url-vumid=${BMW_VUM_ID_URL:http://localhost:8100/api/v1/werkzoekendeProfielen/}
```
waarbij de variabele uit ```${variabele:default waarde}``` zijn waarde krijgt middels de [UM docker compose file][101] en bijbehorende [properties file][102].

Merk het volgende op:

| variabele          | opmerking                                                                                                |
|--------------------|----------------------------------------------------------------------------------------------------------|
| BMW_JWT_ISSUER_URI  | gevuld met een waarde die begint met de KEYCLOAK_FRONTEND_URL zoals eerder in deze documentatie genoemd. |

## 2.2 werkzoekende profielen-bron

### [application properties][552]
```
server.port=${BRW_SERVER_PORT:8086}
server.address=${BRW_SERVER_ADDRESS:0.0.0.0}

logging.level.org.springframework=${BRW_LOGGING_LEVEL:DEBUG}
logging.file.path=${BRW_LOGGING_FILE_PATH:.}
logging.file.name=${BRW_LOGGING_FILE_NAME:app.log}

spring.security.oauth2.resourceserver.jwt.issuer-uri=${BRW_JWT_ISSUER_URI:http://localhost:8083/auth/realms/poc-vng-develop-realm}

spring.h2.console.enabled=${BRW_CONSOLE_ENABLED:true}
spring.datasource.url=${BMW_DATASOURCE_URL:jdbc:h2:./werkzoekende-bron;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE}
spring.datasource.driverClassName=${BRW_DATASOURCE_DRIVER:org.h2.Driver}
spring.datasource.username=${BRW_DATASOURCE_USERNAME:sa}
spring.datasource.password=${BRW_DATASOURCE_PASSWORD:}
spring.jpa.database-platform=${BRW_DATABASE_PLATFORM:org.hibernate.dialect.H2Dialect}
spring.jpa.hibernate.ddl-auto=${BRW_HIBERNATE_DDL:update}

elasticsearch.url=${BRW_ELASTICSEARCH_URL:localhost:8084}

cap.max-amount-response=${BRW_MAX_AMOUNT_RESPONSE:100}
```


## 2.3 vacatures-bron

### [application properties][452]
```
server.port=${BRV_SERVER_PORT:8087}
server.address=${BRV_SERVER_ADDRESS:0.0.0.0}

logging.level.org.springframework=${BRV_LOGGING_LEVEL:DEBUG}
logging.file.path=${BRV_LOGGING_FILE_PATH:.}
logging.file.name=${BRV_LOGGING_FILE_NAME:app.log}

spring.security.oauth2.resourceserver.jwt.issuer-uri=${BRV_JWT_ISSUER_URI:http://localhost:8083/auth/realms/poc-vng-develop-realm}

spring.h2.console.enabled=${BRV_CONSOLE_ENABLED:true}
spring.datasource.url=${BRV_DATASOURCE_URL:jdbc:h2:./vacatures-bron;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE}
spring.datasource.driverClassName=${BRV_DATASOURCE_DRIVER:org.h2.Driver}
spring.datasource.username=${BRV_DATASOURCE_USERNAME:sa}
spring.datasource.password=${BRV_DATASOURCE_PASSWORD:}
spring.jpa.database-platform=${BRV_DATABASE_PLATFORM:org.hibernate.dialect.H2Dialect}
spring.jpa.hibernate.ddl-auto=${BRV_HIBERNATE_DDL:update}

elasticsearch.url=${BRV_ELASTICSEARCH_URL:localhost:8084}

cap.max-amount-response=${BRV_MAX_AMOUNT_RESPONSE:100}
```

## 2.4 vacatures bemiddelaar

### [application properties][402]
```
server.port=${BMV_SERVER_PORT:8089}
server.address=${BMW_SERVER_ADDRESS:0.0.0.0}

logging.level.org.springframework=${BMV_LOGGING_LEVEL:DEBUG}
logging.file.path=${BMV_LOGGING_FILE_PATH:.}
logging.file.name=${BMV_LOGGING_FILE_NAME:app.log}

spring.security.oauth2.resourceserver.jwt.issuer-uri=${BMV_JWT_ISSUER_URI:http://localhost:8083/auth/realms/poc-vng-develop-realm}

spring.h2.console.enabled=${BMV_CONSOLE_ENABLED:true}
spring.datasource.url=${BMV_DATASOURCE_URL:jdbc:h2:./vacatures-bemiddelaar;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE}
spring.datasource.driverClassName=${BMV_DATASOURCE_DRIVER:org.h2.Driver}
spring.datasource.username=${BMV_DATASOURCE_USERNAME:sa}
spring.datasource.password=${BMV_DATASOURCE_PASSWORD:}
spring.jpa.database-platform=${BMV_DATABASE_PLATFORM:org.hibernate.dialect.H2Dialect}
spring.jpa.hibernate.ddl-auto=${BMV_HIBERNATE_DDL:update}

cap.days-to-expiry=${BMV_DAYS_TO_EXPIRE:7}
cap.max-times-accessible=${BMV_MAX_DETAIL_PROFILES:10}
cap.callback-url=${BMV_CALLBACK_URL_BEMIDDELAAR:http://localhost:8081/aanvraagvacature/callback}
cap.vum-url-matches=${BMV_VUM_URL_MATCHES:http://localhost:8200/api/v1/vacatures/matches}
cap.vum-url-vumid=${BMV_VUM_ID_URL:http://localhost:8200/api/v1/vacatures/}

elasticsearch.url=${BMV_ELASTICSEARCH_URL:localhost:8084}

```

## 2.5 Webapplicatie

### [enviornment.ts][302]
```
 production: false,
 gatewayUrl: window\['env'\]\['gatewayUrl'\] || "http://localhost:8081", // kong gateway
 kibanaUrl: window\['env'\]\['kibanaUrl'\] || "http://localhost:8082", // kibana
 jwtIssuerUri: window\['env'\]\['jwtIssuerUri'\] || "http://localhost:8083/auth/realms/poc-vng-develop-realm", // Identity Server
 strictDiscoveryDocumentValidation: window\['env'\]\['strictDiscoveryDocumentValidation'\] || true,
 clientId: window\['env'\]\['clientId'\] || "poc-vng-frontend",
 scope: window\['env'\]\['scope'\] || "openid profile email offline_access",
 requireHttps: window\['env'\]\['requireHttps'\] || false,
``` 
waarbij de variabele uit ```window\['env'\]\['variabele\] || "default waarde"``` zijn waarde krijgt middels de [UM docker compose file][101] en bijbehorende [properties file][102].

Merk het volgende op:

| variabele                         | opmerking                                                                                                                                                                                                                    |
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| strictDiscoveryDocumentValidation | true, tenzij dit voor de specifieke identity server problemen geeft doordat deze het Oauth discovery document uitgeeft met verschillende hosts in de gegeven url's).                                                         |
| requireHttps                      | False om aan te geven dat de webapp over http ipv https met de Identity Server verbinding mag maken. Enkel en alleen false op een ontwikkelomgeving, waarbij de JWT issuer uri niet via localhost en/of HTTPS bereikbaar is. |

[100]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose
[101]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/blob/develop/docker-compose.yml
[102]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/blob/develop/.env

[200]:  https://gitlab.com/vng-realisatie/um-pilot/gateway
[201]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/Dockerfile
[202]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/conf/kong/kong.compose.conf
[203]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/conf/gateway/kong.compose.yml

[2001]: https://docs.konghq.com/hub/kong-inc/cors/
[2002]: https://github.com/nokia/kong-oidc

[250]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak
[251]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak/-/blob/develop/docker/docker-compose.yml
[252]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak/-/blob/develop/docker/.env

[300]:  https://gitlab.com/vng-realisatie/um-pilot/web-applicatie
[302]:  https://gitlab.com/vng-realisatie/um-pilot/web-applicatie/-/blob/develop/src/environments/environment.ts
[310]:  https://gitlab.com/vng-realisatie/um-pilot/web-applicatie/-/blob/develop/src/assets/app-config.json

[400]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bemiddelaar
[402]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bemiddelaar/-/blob/develop/src/main/resources/application.properties

[450]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bron
[452]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bron/-/blob/develop/src/main/resources/application.properties

[500]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bemiddelaar
[502]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bemiddelaar/-/blob/develop/src/main/resources/application.properties

[550]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bron
[552]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bron/-/blob/develop/src/main/resources/application.properties

[600]:  https://gitlab.com/vng-realisatie/um-pilot/camel-adapter
[602]:  https://gitlab.com/vng-realisatie/um-pilot/camel-adapter/-/blob/develop/src/main/resources/application.properties

[700]:  https://gitlab.com/vng-realisatie/um-pilot/elk

[800]:  https://gitlab.com/vng-realisatie/um-pilot/stub-vacatures

[850]:  https://gitlab.com/vng-realisatie/um-pilot/stub-werkzoekendeprofielen

