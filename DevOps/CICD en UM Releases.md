  
# CI en release procedure
Dit document beschrijft de strategie om te komen tot [UM releases met gebruikmaking van CICD principes](#cicd-en-um-releases) voor de PoC UM.

Een beknopt verslag van de technische inrichting is beschreven in het hoodstuk [Inrichting van CICD op Gitlab](#inrichting-van-cicd-op-gitlab).

_Versionering_  

| versie | datum          | opmerking      |
|--------|----------------|----------------|
| 0.1    | augustus 2022  | Initiele opzet |
| 1.0    | september 2022 | Herzien met  beoogde GIT branching strategie|

# CICD en UM Releases
Zie ook [gitlab](https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/blob/develop/doc/CICDenUMReleases.md)

## Situatie
1. Voor de verschillende componenten waaruit de PoC UM is opgebouwd, bestaan aparte Git repositories.
1. De verschillende componenten worden in aparte docker containers gedraaid.
1. De verschillende docker containers worden bijelkaar gehouden door een docker compose, welke onderdeel is van een aparte Git repo.

## Uitgangspunten
* Eenvoudige procedure, die gericht is op Continuous  Integration (CICD) en uitgebreid kan worden met extra controles en automatische stappen afhankelijk van het vervolg van de huidige PoC.
* Bij de beschrijving van dit document wordt uitgegaan dat er gereleased wordt vanaf de main branch. Voorlopig wordt er tijdens de PoC gereleased vanaf de develop branch. De CICD pipelines zijn ingericht op het laatste, eenvoudige aanpassing mogelijk middels de UM_VERSIONBRANCH variabele in de pipeline van het um-compose project.

## Eisen
* [Vanuit ontwikkel oogpunt](#vanuit-ontwikkeloogpunt): Om de laatste code aanwezig op de 'develop' branch in Gitlab te draaien moet een corresponderende image op de docker hub aanwezig zijn.
* [Vanuit release oogpunt](#vanuit-releaseoogpunt): Een release van het project kenmerkt zich door:
    * een project breed gebruikt versienummer van de release
    * alle code is getagged met dit versienummer
    * een image getagged met dit versienummer is aanwezig op docker hub
    * alle noodzakelijke configuratie om het project te laten draaien is aanwezig in de verschillende git repo's. Deze default configuratie wordt ook in de images op docker hub aangebracht, zodat alleen de docker compose file voldoende moet zijn om de UM PoC in de docker omgeving te starten.

De eisen zijn vanuit het oogpunt van [ontwikkeling](#vanuit-ontwikkeloogpunt) en vanuit het oogpunt [releases](#vanuit-releaseoogpunt) zijn in de onderstaande hoofstukken uitgewerkt.

## Vanuit ontwikkeloogpunt
Wanneer code op de developers branch op GitLab wordt aangepast, dan wordt een bij deze GitLab behorende image gemaakt en op docker hub gezet met de tag: 'developer'.

Voorbeeld:
1. Commit code op de develop branch: https://gitlab.com/vng-realisatie/um-pilot/gateway/
1. Na enkele momenten zal een nieuwe pipeline job starten, te zien in https://gitlab.com/vng-realisatie/um-pilot/gateway/-/pipelines
1. Wanneer de pipeline job klaar is, wordt deze geupload (gepushed) naar de docker registry zal een nieuw/vernieuwde  en zal het image te zien zijn in bijvoorbeeld: https://hub.docker.com/r/vngrci/gateway/tags?name=develop
1. Het image bevat ook een aantal labels:  

| Label                             | Waarde (voorbeeld)                                 |
|-----------------------------------|----------------------------------------------------|
| org.opencontainers.image.created  | datum image (2022-09-08T13:27:21)                  |
| org.opencontainers.image.revision | git commit hash (a70b4df3af4fcd46dd...)            |
| org.opencontainers.image.title    | image name (gateway)                               |
| org.opencontainers.image.url      | repo url (https://gitlab.com/.../gateway)          |
| org.opencontainers.image.version  | git branch (develop)                               |

De url en de revision geven aan op basis van welke broncode het image is gebouwd.  

![dockerhub-imagelabels.png](CICD%20en%20UM%20Releases-images/dockerhub-imagelabels.png "dockerhub-imagelabels.png")

ad 3. Afbeelding Docker registry  
![dockerhub-gateway-developtag.png](CICD%20en%20UM%20Releases-images/dockerhub-gateway-developtag.png)

## Vanuit releaseoogpunt

### Geslaagde release
Een TODO is beschrijven hoe een release vanuit gebruiksperspectief geverifieerd moet worden, waaraan bijvoorbeeld release documentatie moet voldoen. Vooralsnog wordt de release alleen vanuit technisch proces beoordeeld en moet hierbij gezien worden als een hulpmiddel om tot een betere release te komen, vooralsnog is het geen waterdicht proces.

Een release is voltooid (geslaagd) indien:
* Docker images zijn aanwezig op docker hub, met het UM versienummer. 
* De git repos met de broncode waarmee de images zijn gebouwd, zijn voorzien van een tag met het UM versienummer.
* De docker compose file die de verschillende images samen kan opstarten in docker moet aanwezig en valide (zodanig dat deze op minst door docker ingelezen kan worden) zijn.

### Release proces

![GitBranching.png](CICD%20en%20UM%20Releases-images/GitBranching.png "GitBranching.png")

Het bovenstaande plaatje geeft de _beoogde_ git branching strategie weer.  
Broncode wordt door de ontwikkelaar op de develop branch gecommit. Zoals beschreven in de vorige paragraaf, wordt vervolgens automatisch een docker image gemaakt met de tag "develop" en wordt deze op in docker registry gezet.  

In aanloop naar de release wordt de code naar de main branch gemerged. Bij elke merge wordt een docker image gemaakt en in docker geregistery gezet. Dit gaat op precies dezelfde wijze als voor de images gebouwd vanaf de 'develop' branch, alleen krijgen deze de tag 'main', i.p.v 'develop'.  
De opgeleverde code en images moeten gecontroleerd/getest worden, ze worden gebruikt voor de uiteindelijk release, het is daarom van belang dat het mergen gecontroleerd plaats vindt (denk bv aan een code freeze gecoordineerd door iemand in de rol van release manager).

Wanneer de release een 'go' heeft dienen de volgende stappen plaats te vinden: 
1. (Handmatig) Bepaal nieuwe versienummer voor het UM project. Moet beginnen met "UM", bijvoorbeeld: UM1.0
1. (Handmatig) Start het automatische release proces, met de variabele "umversie", gevuld met het nieuwe versienummer
1. (Automatisch) Voor elke repo afzonderlijk:
   * De docker images op docker hub met de tag “main” wordt gedupliceerd. Het duplicaat krijgt de tag van het UM versie nummer
   * De git repo wordt getagged met het UM versienummer, gebruik makend van de “git commit hash” opgeslagen bij het docker image onder label "org.opencontainers.image.revision", zodat precies die code wordt getagged, die gebruikt is voor het aanmaken van het docker image.
1. (Automatisch) Het UM-compose project wordt getagged het versie nummer.
1. (Handmatig) Controleer of de pipeline netjes is afgerond
   * De status van de [pipeline](https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/pipelines) is 'passed'
   * De release is aanwezig op [gitlab](https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/releases/)

ad 2: Starten van release pipeline met umversie als variabele.  

> _Run for branch name or tag_: dit geeft aan waar de pipeline code staat (welke pipeline code gebruikt wordt), niet van welke branch een gerelease wordt gemaakt. Dat laatste is namelijk vastgelegd in de pipeline.  
> _umversie_ moet als variabele toegevoegd worden. De waarde moet beginnen met "UM".  

![releasePipelineStarten.png](CICD%20en%20UM%20Releases-images/releasePipelineStarten.png "releasePipelineStarten.png")

ad 5: gitlab-releases page
![gitlab-releasespage.png](CICD%20en%20UM%20Releases-images/gitlab-releasespage.png "gitlab-releasespage.png")

# Inrichting van CICD op Gitlab

## Pipeline files
Deze files bevatten de instructies voor de automatisch stappen.
Een beknopte beschrijving volgt hieronder.

#### um-compose [gitlab-ci.yml](https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/ci/editor?branch_name=develop)
De pipeline voor ter ondersteuning van het [release proces](#release-proces) zoals hierboven beschreven, wordt gedefinieerd door de file gilab-ci.yml in de root van de git repository um-compose en is ook vanuit het CICD menu item in gitlab te benaderen. 

De pipeline dient handmatig gestart te worden. De variable "umversie" moet worden meegegeven:
 
De pipeline is opgedeeld in stages welke in de onderstaande volgorde doorlopen worden:

Stages
1. prepare - controleert of de umversie reeds gebruikt is als tag op repo um-compose
2. package - dupliceert bestaande images (main tag) en geeft deze de tag van de um versie. Tag de broncode in gitlab, die bij de images hoort.
3. verify_release - controleert of alle tags zijn aangemaakt. Controleert de docker compose file.
4. finalize_release - Tag het um-compose project met het um versie nummer.

Elke stage bevat één of meerdere jobs. De stages en jobs kunnen visueel worden weergegeven (Visualize tab in de CICD editor):
![PipelineEditor-visualize.png](CICD%20en%20UM%20Releases-images/PipelineEditor-visualize.png)

####  Overige git projecten, bv. gateway [gitlab-ci.yml](https://gitlab.com/vng-realisatie/um-pilot/gateway/-/ci/editor?branch_name=develop)
In de voorgaande paragraaaf is de pipeline van het um-compose project besproken, De andere git repostories bevatten ook een gitlab-ci.yml dat de de pipeline definieerd.

Deze pipeline zorgt het aanmaken en pushen van een docker image naar docker hub:
* automatisch nadat nieuwe code is gecommit op de develop branch. De docker image wordt getagged met zowel 'latest' als 'develop' (twee images dus)

Omdat de verschillende repos grootendeels dezelfde stappen doorlopen wordt een file geimporteerd die zich bevindt in het um-compose project.

## Configuratie settings
Configuratie in Gitlab ten behoeve van CICD

| Git lab repo          | Wat                           | Locatie                                                                                  | Detail                                       |
|-----------------------|-------------------------------|------------------------------------------------------------------------------------------|----------------------------------------------|
| um-compose            | Connectie gegevens docker hub | [CICD variables](https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/settings/ci_cd) | CI_REGISTRY                                  |
|                       |                               |                                                                                          | CI_REGISTRY_PASSWORD (deze moet masked zijn! |
|                       |                               |                                                                                          | CI_REGISTRY_USER                             |
| Overige git projecten | Connectie gegevens docker hub | [CICD variables](https://gitlab.com/vng-realisatie/um-pilot/gateway/-/settings/ci_cd)    | CI_REGISTRY_USER                             |
|                       |                               |                                                                                          | CI_REGISTRY_PASSWORD (deze moet masked zijn! |
|                       |                               |                                                                                          | CI_REGISTRY_USER                             |
|                       | Naam docker image             |                                                                                          | CI_REGISTRY_IMAGE                            |

# Wensen
De CICD pipelines zijn gerealiseerd binnen een PoC en daarom eenvoudig gehouden. Om de CICD verder te verbeteren, kan gedacht worden aan:
* Automatische release notes
* Daadwerkelijke deployment toevoegen.
* Automatisch testen
  * Een initiele opzet voor het automatisch testen van api's is uitgewerkt in de pipeline van de git repo: https://gitlab.com/vng-realisatie/um-pilot/test/-/pipelines. Deze pipeline demonstreert momenteel niet meer dan dat de testen op Gitlab gedraaid kunnen worden.
* Omdat in de huidige fase van het project nog niks wordt gedaan met de main branch, worden alle releases gebouwd vanaf de develop branch. Dit kan eenvoudig worden aangepast, middels het veranderen van de variabele voor de release branch in het pipeline script van um-compose.
* De verschillende CICD configuratie settings moeten ook op group niveau gedaan kunnen worden ipv dezelfde parameters bij elk onderliggend project, echter heeft de schrijver van dit document daar geen toegang toe.
