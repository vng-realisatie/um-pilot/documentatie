# Als gateway wil ik een OIN specifieke client certificaat presenteren

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | december 2022 | TODO      |


### Inleiding
Deze user story is randvoorwaardelijk voor een goedwerkend authenticatie en autorisatie mechanisme en heeft als doel om uitsluitend aangesloten partijen toegang te geven tot VUM.
Ondersteuen multi tenancy
### Sectie Functioneel

Wanneer een OIN nummer in de header wordt meegegeven vanuit de aanroepen partij, dan moet op basis van dit nummer een certificaat worden meegeven bij de uitgaande call.

### Sectie Technisch (optioneel)
hierin wordt de techniek nader toegelicht

### Acceptatiecriteria 

Todo Uitwerken

API Gateway (Kong) token validatie bij IAM (Keycloak)
Als de gebruiker een token heeft gekregen, moet deze na een actie binnen het medewerkersportaal nogmaals gecontroleerd worden op geldigheid en time-out.

Client certificaat koppelen aan uitgaande API calls, binnenkomende API calls afschermen zodat er maar één client certificaat toegang heeft tot die specifieke API call. 
Denk aan: VUM die een vraag stelt aan UM.