# Als bemiddelaar wil ik werkzoekenden aan vacatures kunnen koppelen zodat ik werkzoekenden een passende aanbieding kan doen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |

# Inleiding

Dit document beschrijft in hoofdlijnen de functionaliteit van UM-W.
De reikwijdte van de functionaliteit van UM-W betreft het ter beschikking stellen van componenten om VUM te benaderen. 
Deze componenten zijn in hoge mate pluggable. UM-W zelf bevat weinig tot geen Business Logica maar stelt de API's van VUM op een laag drempelige manier open.
Dit uit zich ook in de uitwerking van de Features, de User Stories en de acceptatie criteria.
Waar deze op feature niveau zich nog uitdrukken in functionele termen, is er op lager niveau meer aandacht voor de technische uitwerking.  

# Functioneel
In hoofdlijnen is UM-W een stelsel aan applicaties die het mogelijk maakt vacatures aan werkzoekenden te koppelen en vice versa. 
Een drijvend concept achter UM-W is het 'Common Ground' principe [^Common Ground].
UM-W stelt enerzijds bemiddelaars in staat om werkzoekenden aan te bieden aan UM-W en anderzijds om vacatures aan te bieden.

De volgende Use Cases zijn onderscheiden [^Bron]:

9. <font color="grey">Als gemeente lever ik handmatig profielen via de UM GUI om mijn bestanden beschikbaar te maken binnen VUM. (UC1)</font> [^OutOfScope]

10. Als gemeente upload ik de werkzoekendeprofielen in Sonar formaat via een bestandsupload in de UM GUI om mijn profielen beschikbaar te maken binnen VUM.(UC3)
    * Als Gemeente wil ik werkzoekendenprofielen kunnen aanbieden aan VUM ([Upload Werkzoekenden](/User%20Stories/Functioneel/Werkzoekende/Als%20Gemeente%20wil%20ik%20werkzoekendenprofielen%20kunnen%20aanbieden%20aan%20VUM.md))

15. Als VUM werkzoekendenprofielen opvraagt zorgt UM dat de juiste antwoorden worden gegeven om een match mogelijk te maken. (UC4)
16. Als VUM detailprofielen opvraagt zorgt UM dat de juiste antwoorden worden gegeven (UC5)
17. Als gemeente stel ik een vacature zoekvraag via de UM GUI, om een passende vacature te vinden. (UC6)
18. Als gemeente stel ik een detailvraag via de UM GUI, om op basis van een passende vacature contact te kunnen leggen. (UC8)

25.	Als gemeente kan ik een zoekvraag stellen via de gemeentelijke leverancier en worden antwoorden via API geleverd in de interface van de leverancier. Zodat ik niet hoef te schakelen tussen verschillende systemen. (UC7)
26.	Als gemeente kan ik een detailvraag stellen via de gemeentelijke leverancier en worden de detail antwoorden via de API getoond in de interface van de leverancier. Zodat ik niet hoef te schakelen tussen verschillende systemen. (UC9)

28. Gemeente kan in de rapportage zien/downloaden welke profielen/vacatures door wie geraadpleegd zijn. (UC11 & 12)
29. Als gemeente stel ik een werkzoekende zoekvraag via de UM GUI, om een passend werkzoekende profiel te vinden.
30. Als gemeente stel ik een detailvraag via de UM GUI, om op basis van een passende werkzoekende profiel contact te kunnen leggen.
31. Als gemeente kan ik mijn gebruikers beheren via het beheerportaal

[^Common Ground] [Common Ground](https://vng.nl/artikelen/common-ground)  
[^Bron]  
[Trello](https://trello.com/b/AVKsUzC5/um-pilot)  
[Inrichting uitwisselingsmechanisme gemeenten](UM-UitwisselingsMechanisme/Gedeelde%20documenten/Techniek/Documentatie%20UM/011221%20Inrichting%20uitwisselingsmechanisme%20gemeenten%20versie%204.1%20mod%20by%20TvdM%20en%20SM.docx?d=w6e17ce1b080e418e977af38de925c5eb&csf=1&web=1&e=zRhiKy)
[^OutOfScope] Deze UC is vooralsnog Out of Scope geplaatst.

# Technisch

UM-W bestaat uit drie componenten die onafhankelijk van elkaar ingezet kunnen worden :

1. De API - deze bevat de core functionaliteit van UM-W
2. De UI - deze bevat een 'dunne' schil en dient om de API te raadplegen en resultaten weer te geven
3. De Vertaalmodule - deze biedt de mogelijkheid aan overheidspartijen  

Daarnaast zijn er nog enkele componenten die eventueel ook door eigen implementaties vervangen kunnen worden
4. Een identity provider (momenteel wordt gebruik gemaakt van Keycloak)
5. De API-Gateway (momenteel wordt gebruik gemaakt van Kong)
6. De database (Work in Progress, momenteel is dit een in-memory h2 database)
7. De rapportage tool (Work in Progress, momenteel een ELK [^ELK] stack)

[^ELK] Elastich Search, Logstash, Kibana 

### Acceptatiecriteria

Niet van toepassing op dit niveau
