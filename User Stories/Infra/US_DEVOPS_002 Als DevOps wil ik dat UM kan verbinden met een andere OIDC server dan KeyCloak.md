# US_DEVOPS_002 Als DevOps wil ik dat UM kan verbinden met een andere OIDC server dan KeyCloak

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | februari 2023 | initiele opzet |
| 0.10   | maart 2023    | definitief     |


**Als** devops  
**wil ik** wil ik dat UM kan verbinden met een andere OIDC server dan KeyCloak
**zodat** de gemeente haar eigen identity provider kan gebruiken

### Functioneel ###
nvt

### Technisch ###
Wanneer UM (en specifiek ook de Webapp) met een andere Identity Server moet worden verbonden dan moet alle UM componenten naar deze server verwijzen. Dit ticket gaan alleen over de web app front end.

Uit een test waarin Um met Azure AD is verbonden, is naar voren gekomen dat de volgende vars dynamisch gemaakt moeten worden (file auth.service.ts):
- clientId
- scope
- strictDiscoveryDocumentValidation, nieuwe variablele. Default waarde true. Voor azure false.

Keycloak buiten de um compose file plaatsen.
Wel gebruik maken van een compose file, zodat eenduidige werkwijze ontstaat.
Acceptatie hierop aansluiten
Stukje documentatie.
Een realm voor test en acceptatie vastgelegd in een json file.

[keycloak](https://gitlab.com/vng-realisatie/um-pilot/keycloak)
[verdere documentatie](https://gitlab.com/vng-realisatie/um-pilot/documentatie/-/blob/develop/DevOps/opzetten%20UM%20in%20Docker.md)

Nice to have:
- variabele maken waar de rollen in het token staan, zie auth.service.ts (r67,r68) en nav-bar.component.ts
  Als hier geen ala minuut oplossing voor is, dan is het beter dit voor 'later' te bewaren.