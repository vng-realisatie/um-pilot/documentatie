# US_DEVOPS_003 Als devOps wil ik een andere DB kunnen gebruiken

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | februari 2023 | initiele opzet |
| 0.10   | maart 2023    | definitief     |


**Als** devops  
**wil ik** een andere DB kunnen gebruiken
**zodat** de gemeente haar eigen identity provider kan gebruiken

### Functioneel ###
nvt

### Technisch ###
Dit betreft het aantonen dat grotendeels middels configuratie het mogelijk is van database te kunnen wisselen.
Hier toe is naast een H2 database ook een postgresql database gebruikt

zie ook [trello](https://trello.com/c/ajDxKDNI)
