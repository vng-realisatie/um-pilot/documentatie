# Als Gemeente wil ik UM-W als Bron kunnen gebruiken voor het opslaan van gegevens

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | december 2022 | TODO      |


### Inleiding

UM-W kan op verschillende wijzen ingezet worden door gemeenten. Gemeentes zijn vrij om een eigen database te gebruiken als bron voor VUM.
Gemeentes kunnen er ook voor kiezen de UM-W database te gebruiken als Bron.
Ondersteunen multi tenancy

### Sectie Functioneel

Omdat meerdere gemeentes gebruik kunnen maken van de UM-W database is het noodzakelijk de database multi-tenant in te richten. 
Gegevens van de verschillende gegevens worden gescheiden van elkaar op basis van het OIN.

### Sectie Technisch (optioneel)
Voor de duur van de POC is gekozen voor een h2 in-memory database

### Acceptatiecriteria 

*Feature: in mem database*
**Gegeven** in de desbetreffende environment variabeles is h2 als database gedefinieerd   
**Wanneer** de applicatie wordt gestart  
**Dan** wordt gebruik gemaakt van de h2 in-mem database
**En** is de applicatie beschikbaar

*Feature: mariaDB/mySql*
**Gegeven** in de desbetreffende environment variabeles is mariaDB/mySql als database gedefinieerd   
**Wanneer** de applicatie wordt gestart  
**Dan** wordt gebruik gemaakt van de mariaDB/mySql database
**En** is de applicatie beschikbaar

*Feature: verkeerde config*
**Gegeven** in de desbetreffende environment variabeles is de database onvolledig of foutief gedefinieerd   
**Wanneer** de applicatie wordt gestart  
**Dan** faalt het opstarten  
**En** is de applicatie niet beschikbaar 