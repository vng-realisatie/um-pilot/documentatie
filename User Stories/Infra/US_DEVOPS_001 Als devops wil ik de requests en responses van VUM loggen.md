# US_DEVOPS_001 Als devops wil ik de requests en responses van VUM loggen

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | februari 2023 | initiele opzet |


**Als** devops  
**wil ik** de requests en responses van VUM loggen  
**zodat ik** kan achterhalen welke berichten uitgewisseld zijn

### Functioneel ###
nvt

### Technisch ###
Om te kunnen achterhalen welke 1.0.0 versie door OAS is geimplementeerd (helaas zijn er meerdere) is het noodzakelijk om het binnengekomen response object inclusief header te loggen.
Tegelijkertijd is het ook wenselijk het bijbehorende request (incl header) te loggen.
Dit kan in de reguliere logging (loglevel debug), hoeft niet opgenomen te worden in ELK