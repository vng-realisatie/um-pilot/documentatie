# Feature : US_VMD_010 Als Gegevensverantwoordelijke wil ik gegevens kunnen versturen naar VUM

_Versionering_

| versie | datum        | opmerking      |
|--------|--------------|----------------|
| 0.10   | januari 2023 | Initiele opzet |


**Als** Gegevensverantwoordelijke  
**wil ik** gegevens up kunnen loaden vanuit een spreadsheet
**zodat ik** 

Functioneel
----------------
todo 

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature:*  
**Gegeven**   
**Wanneer**
**Dan** 

*Scenario: Geen authorisatie*  
**Gegeven**   
**Wanneer**
**Dan**
