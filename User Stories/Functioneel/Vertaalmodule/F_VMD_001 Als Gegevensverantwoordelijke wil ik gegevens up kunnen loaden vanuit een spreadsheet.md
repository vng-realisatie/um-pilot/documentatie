# Feature : Als Gegevensverantwoordelijke wil ik gegevens up kunnen loaden vanuit een spreadsheet

_Versionering_

| versie | datum        | opmerking      |
|--------|--------------|----------------|
| 0.10   | januari 2023 | Initiele opzet |


**Als** Gegevensverantwoordelijke  
**wil ik** gegevens up kunnen loaden vanuit een spreadsheet
**zodat ik** 

Functioneel
----------------
10-7 Externe vertaalmodule werkzoekendeprofielen van Sonar --> VUM  
Dit ticket bevat voorbeeld bestanden

todo 

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature:*  
**Gegeven**   
**Wanneer**
**Dan** 

*Scenario: Geen authorisatie*  
**Gegeven**   
**Wanneer**
**Dan**
