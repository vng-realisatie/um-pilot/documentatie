# UM-Pilot

# Functionele Documentatie Werkzoekenden

### UC4: VUM uitwisselingsvoorziening vraagt matchingsprofielen op ###

De bevraagbare bron per gemeente in het UM kan geraadpleegd worden om matchingsprofielen op te vragen. 
De VUM uitwisselingsvoorziening dient een zoekvraag in aan het UM, gericht aan een specifieke gemeente. 
Het UM zoekt de matchingsprofielen die voldoen aan de criteria in de gestelde vraag. 
Deze matchingsprofielen worden vervolgens beschikbaar gesteld aan de VUM uitwisselingsvoorziening. 
Met UC5 kunnen vervolgens detailprofielen opgevraagd worden. 
VUM houdt bij welke vragen aan welke gemeente (op OIN) gesteld moeten worden. 
Het kan dus zijn dat dezelfde vraag aan twee UM-gemeenten gesteld wordt vanuit VUM. 
Binnen UM zijn dat twee onafhankelijk te verwerken vragen.

### Features ###
F-15. Als VUM werkzoekendenprofielen opvraagt zorgt UM dat de juiste antwoorden worden gegeven om een match mogelijk te maken. (UC4)

Zie ook : https://trello.com/c/QBiPtvEL/1-15-als-vum-werkzoekendenprofielen-opvraagt-zorgt-um-dat-de-juiste-antwoorden-worden-gegeven-om-een-match-mogelijk-te-maken-uc4

#### User stories ####


#### Acceptatiecriteria ####
Voor het accepteren van de onderliggende user stories wordt uitgegaan van een gebruiker die is ingelogd met de juiste rechten. Userstoriesmet betrekking tot het inloggen en de afhandeling van toekennen van rechten zijn <hier TODO> beschreven 
