# Feature : F_WZB_001 Als Profielverantwoordelijke wil ik een bron voor werkzoekendenprofielen ter beschikking stellen

versie 0.10

_Versionering_

| versie | datum        | opmerking                             |
|--------|--------------|---------------------------------------|
| 0.10   | januari 2023 | Initiele opzet                        |

**Als** Profielverantwoordelijke  
**Wil ik** een bron voor werkzoekendenprofielen ter beschikking stellen  
**Zodat ik** een default applicatie aan kan bieden

### Functioneel 

Om werkzoekenden te kunnen koppelen aan vacatures dienen de profielen van werkzoekenden beschikbaar te zijn in VUM.
Hiertoe dient het mogelijk te zijn om een (verzameling van) werkzoekendenprofiel(en) aan de bron toe te voegen, te raadplegen, te wijzigen of te kunnen verwijderen.

Dit is een grotendeels technische feature die betrekking heeft op het beschikbaar stellen van een default implementatie van UM-W.
Werkzoekende profielen worden opgeslagen in een bron die ontsloten is middels REST services. 
Deze feature beschrijft de eisen waaraan deze bron dient te voldoen.  

Gemeentes kunnen er voor kiezen gebruik te maken van de default implementatie van UM-W. 
Ook kunnen zij er voor kiezen hun eigen applicatielandschap in te zetten.  
In het eerste geval voorziet UM-W erin een bron applicatie voor werkzoekende profielen ter beschikking te stellen.
De applicatie werk in dit geval 'out-of-the-box'.

In het tweede geval is de gemeente zelf verantwoordelijk voor het implementeren van de API die de bron ontsluit.
Voor een goed werkende aansluiting van een eigen implementatie op VUM wordt geadviseerd deze te implementeren volgens de riichtlijnen in deze feature.

### User Stories

#### Bron
- US_WZB_001 Als Profielverantwoordelijke wil ik in bulk werkzoekendenprofielen kunnen aanbieden aan de bron
- US_WZB_002 Als Profielverantwoordelijke wil ik werkzoekendenprofielen kunnen opvragen bij de bron
- US_WZB_003 Als Gebruiker wil ik de detailsgegevens van een werkzoekendenprofiel kunnen opvragen bij de bron
- US_WZB_004 Als Bemiddelaar wil ik werkzoekendenprofielen vinden die aan de matchingscriteria voldoen
- 
#### UI
TODO

### Technische Documentatie

De werkzoekende bron is ontsloten middels een rest-api.
Dit maakt de achterliggende implementatie onafhankelijk voor de buitenwereld.
De yaml die de api beschrijft is te vinden in [gitlab](https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bron/-/blob/develop/doc/UM-Gemeente-WerkzoekendenProfielen-0.8.7.yaml)

De profielen worden vertaald naar de VUM gegevensstandaard (Sonar formaat) en opgenomen in de bevraagbare bron.
Het mechanisme bevat controles om toevoegen van dubbele profielen te voorkomen.
