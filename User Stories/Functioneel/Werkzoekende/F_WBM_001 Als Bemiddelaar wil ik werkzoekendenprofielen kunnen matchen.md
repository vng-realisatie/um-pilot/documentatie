# Feature : F_WBM_001 Als Bemiddelaar wil ik werkzoekendenprofielen kunnen matchen
_Versionering_

| versie  | datum         | opmerking      |
|---------|---------------|----------------|
| 0.10    | februari 2023 | Initiele opzet | 
| 0.10    | maart 2023    | Definitief     | 

**Als** Bemiddelaar  
**wil ik** werkzoekendenprofielen kunnen matchen
**zodat** ik een werkzoekende een passende vacature aan kan bieden

### Functioneel
Bemiddelaars kunnen aan de hand van een aantal criteria werkzoekendeprofielen opvragen. 
Een dergelijke 'vraag' aan de applicatie wordt opgeslagen en gedurenden een bepaalde periode bewaard. 
Na deze periode komt de vraag en de daarbij bijbehorende gevonden werkzoekendeprofielen te vervallen. 
Van de gevonden werkzoekendeprofielen kan een beperkte set geraadpleegd worden.

#### User Stories
De userstories zijn verdeeld over 2 secties. 
De eerste sectie betreft het aanbieden van de functionaliteit via API's.
Daarnaast is er een user interface beschikbaar

#### Bemiddelaar (API)
Deze userstories zijn beschreven in het werkzoekendeprofielen-bemiddelaar project. Hieronder een opsomming van deze userstories

- US_WBM_001 Als Bemiddelaar wil ik een aanvraag naar werkzoekendeprofielen kunnen registreren
- US_WBM_002 Als Bemiddelaar wil ik bestaande aanvragen naar werkzoekendeprofielen kunnen raadplegen
- US_WBM_003 Als Bemiddelaar wil ik een bestaande werkzoekendeprofielen aanvraag kunnen raadplegen
- US_WBM_004 Als Bemiddelaar wil ik de detailgegevens van een werkzoekendeprofielen aanvraag kunnen raadplegen
- US_WBM_010 Als beheerder wil ik controle hebben over het geautomatiseerde bewaarregime op werkzoekendeprofielen


#### UI
Deze userstories zijn beschreven in het ui project. Hieronder een opsomming van deze userstories

- US_UI_005 Als profielverantwoordelijke wil ik een overzicht van de geuploade werkzoekendenprofielen kunnen raadplegen
- US_UI_011 Als bemiddelaar wil ik werkzoekendeprofielen kunnen opvragen
- US_UI_012 Als Gebruiker wil ik een werkzoekendenprofiel kunnen raadplegen

### Technische Documentatie
De werkzoekendeprofielen bemiddelaar is ontsloten middels een rest-api.
Dit maakt de achterliggende implementatie onafhankelijk voor de buitenwereld.

- Voor de technische documentatie wordt verwezen naar
[Technisch Ontwerp](../../../Technisch%20Ontwerp/userstories.md)