# Feature : Als Bemiddelaar wil ik vacatures kunnen behandelen
_Versionering_

| versie  | datum         | opmerking                                                 |
|---------|---------------|-----------------------------------------------------------|
| 0.1     | december 2022 | Initiele opzet                                            |
| 0.10    | februari 2023 | Aanscherping documentatie met verwijzing naar Userstories | 



### Functioneel
Bemiddelaars kunnen aan de hand van een aantal criteria vacatures opvragen. 
Een dergelijke 'vraag' aan de applicatie wordt opgeslagen en gedurenden een bepaalde periode bewaard. 
Na deze periode komt de vraag en de daarbij bijbehorende gevonden vacatures te vervallen. 
Van de gevonden vacatures kan een beperkte set geraadpleegd worden.

#### User Stories
De userstories zijn verdeeld over 2 secties. 
De eerste sectie betreft het aanbieden van de functionaliteit via API's.
Daarnaast is er een user interface beschikbaar

#### Bemiddelaar (API)
Deze userstories zijn beschreven in het vacatures-bemiddelaar project. Hieronder een opsomming van deze userstories

- US_VBM_001 Als Bemiddelaar wil ik een aanvraag naar vacatures kunnen registreren  
- US_VBM_002 Als Bemiddelaar wil ik bestaande aanvragen naar vacatures kunnen raadplegen  
- US_VBM_003 Als Bemiddelaar wil ik een bestaande vacature aanvraag kunnen raadplegen  
- US_VBM_004 Als Bemiddelaar wil ik de detailgegevens van een vacature aanvraag kunnen raadplegen
- US_VBM_010 Als beheerder wil ik controle hebben over het geautomatiseerde bewaarregime op vacatures

#### UI
Deze userstories zijn beschreven in het ui project. Hieronder een opsomming van deze userstories

TODO

### Technische Documentatie
De vacatures bemiddelaar is ontsloten middels een rest-api.
Dit maakt de achterliggende implementatie onafhankelijk voor de buitenwereld.

- Voor de technische documentatie wordt verwezen naar
[Technisch Ontwerp](../../../Technisch%20Ontwerp/userstories.md)