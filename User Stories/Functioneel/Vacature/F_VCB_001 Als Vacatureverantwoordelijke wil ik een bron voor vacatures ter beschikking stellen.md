# Feature : F_VAC_001 Als Vacatureverantwoordelijke wil ik een bron voor vacatures ter beschikking stellen

versie 0.10

_Versionering_

| versie | datum        | opmerking                             |
|--------|--------------|---------------------------------------|
| 0.10   | januari 2023 | Initiele opzet                        |

**Als** Vacatureverantwoordelijke  
**Wil ik** een bron voor vacatures ter beschikking stellen  
**Zodat ik** een default applicatie aan kan bieden

### Functioneel 

Om werkzoekenden te kunnen koppelen aan vacatures dienen de vacatures van werkgevenden beschikbaar te zijn in VUM.
Hiertoe dient het mogelijk te zijn om een (verzameling van) vacature(s) aan de bron toe te voegen, te raadplegen, te wijzigen of te kunnen verwijderen.

Dit is een grotendeels technische feature die betrekking heeft op het beschikbaar stellen van een default implementatie van UM-W.
Vacatures worden opgeslagen in een bron die ontsloten is middels REST services. 
Deze feature beschrijft de eisen waaraan deze bron dient te voldoen.  

Gemeentes kunnen er voor kiezen gebruik te maken van de default implementatie van UM-W. 
Ook kunnen zij er voor kiezen hun eigen applicatielandschap in te zetten.  
In het eerste geval voorziet UM-W erin een bron applicatie voor vacatures ter beschikking te stellen.
De applicatie werk in dit geval 'out-of-the-box'.

In het tweede geval is de gemeente zelf verantwoordelijk voor het implementeren van de API die de bron ontsluit.
Voor een goed werkende aansluiting van een eigen implementatie op VUM wordt geadviseerd deze te implementeren volgens de richtlijnen in deze feature.

### User Stories
De userstories zijn verdeeld over 2 secties.
De eerste sectie betreft het aanbieden van de functionaliteit via API's.
Daarnaast is er een user interface beschikbaar

#### Bron
- US_VCB_001 Als Vacatureverantwoordelijke wil ik vacatures kunnen aanbieden aan de bron
- US_VCB_002 Als Vacatureverantwoordelijke wil ik vacatures kunnen opvragen bij de bron
- US_VCB_003 Als Gebruiker wil ik de detailgegevens van een vacature kunnen opvragen bij de bron
- US_VCB_004 Als Bemiddelaar wil ik vacatures vinden die aan de matchingscriteria voldoen
-
#### UI
TODO

### Technische Documentatie

De vacatures bron is ontsloten middels een rest-api.
Dit maakt de achterliggende implementatie onafhankelijk voor de buitenwereld.
De yaml die de api beschrijft is te vinden in [gitlab](https://gitlab.com/vng-realisatie/um-pilot/vacatures-bron/-/blob/develop/doc/UM-Gemeente-Vacatures-0.8.2.yaml)

De profielen worden vertaald naar de VUM gegevensstandaard (Sonar formaat) en opgenomen in de bevraagbare bron.
Het mechanisme bevat controles om toevoegen van dubbele vacatures te voorkomen.
