# Begrippen
versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |

# Inleiding

Dit document beschrijft de begrippen die in UM-W gebruikt worden.

| Begrip                       | Betekenis                                                                                                                                                         | opmerking                                                                |
|------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
| Bron                         | Een bron in de betekenis zoals die is neergelegd in Common Ground.  <br/>UM-W kan binnen VUM fungeren als originele bron voor vacatures en werkzoekendenprofielen | Zie hiervoor ook [Common Ground](https://vng.nl/artikelen/common-ground) |
| Vertaalmodule                | Een module buiten VUM die de vertaling van een excel bestand naar een JSON structuur verzorgt                                                                     |                                                                          |
| UM                           | UitwisselingsMechanisme voor Gemeenten                                                                                                                            |                                                                          |
| UMW                          | UitwisselingsMechanisme Werk                                                                                                                                      |                                                                          | 
| VUM                          | Verbeteren Uitwisseling Matchingsgegevens                                                                                                                         |                                                                          |
| Gegevensverantwoordelijke    | Rol : persoon verantwoordelijk voor het bijhouden van gegevens (werkzoekendeprofielen en vacatures)                                                               |                                                                          |
| Verwerkingsverantwoordelijke | Rol : persoon verantwoordelijk voor de correcte verwerking van gegevens                                                                                           |                                                                          |
| Gebruiker                    | Rol : generieke aanduiding voor een persoon of applicatie die gebruik maakt van UM-W waarbij geen specifieke rol van toepassing is                                |                                                                          |
| Client                       | Rol : de persoon of applicatie die een API direct aanspreekt                                                                                                      |                                                                          |
| Vacatureverantwoordelijke    | Rol : persoon verantwoordelijk voor het bijhouden van vacaturegegevens                                                                                            |                                                                          |
| Profielverantwoordelijke     | Rol : persoon verantwoordelijk voor het bijhouden van werkzoekende profielgegevens                                                                                |                                                                          |
