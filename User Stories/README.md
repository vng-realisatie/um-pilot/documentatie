# UM-Pilot

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |

# Opzet documentatie
UM-W heeft als doel VUM [^VUM] beschikbaar te stellen voor aangesloten partijen. 
Dit zijn overheidspartijen zoals bv gemeenten, maar kunnen ook markpartijen zijn.
Het is een reference implementatie waarin grote vrijheid is bij partijen om onderdelen naar eigen inzicht in te zetten.
De drijvende kracht hierachter is het beschikbaar stellen van UM-W op een manier die aansluit bij de behoefte en de wensen van de gemeente.

Dit document geeft richtlijnen waar de documentatie aan dient te voldoen.
Dit uit zich ook in de uitwerking van de Features, de User Stories en de acceptatie criteria.
Waar deze op feature niveau zich nog uitdrukken in functionele termen, is er op lager niveau meer aandacht voor de technische uitwerking.  

De documentatie is opgezet volgens een vast format. 

# Header 
De header geeft de gehele tekst van de User story / Feature weer. 
B.v. **Als**  <Rol> **Wil ik** <doel> **Zodat** <justificatie> 

_Sectie versionering_ 
hierin wordt een overzicht gegeven van de wijzigingen per versie. Deze loopt gelijk met de versionering van de releases. 

### Sectie Inleiding (optioneel) 
een toelichting op het document. Deze sectie is optioneel

### Sectie Functioneel
hierin wordt de functionaliteit nader toegelicht 

### Sectie Technisch (optioneel)
hierin wordt de techniek nader toegelicht 

### Acceptatiecriteria 
de criteria waaraan de opgeleverde software dient te voldoen om in productie genomen te kunnen worden. Voor UM-W vallen een aantal soorten acceptatie criteria te onderscheiden :
   * Acceptatiecrietria op API-niveau - dit is de kern van UM-W 
   * Acceptatiecrietria op functioneel-niveau - deze hebben met name betrekking op de UI
   * security 
   * infra