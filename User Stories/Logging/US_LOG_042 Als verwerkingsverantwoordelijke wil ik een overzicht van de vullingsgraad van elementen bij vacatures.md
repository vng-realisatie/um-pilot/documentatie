# Feature : US_LOG_041 Als verwerkingsverantwoordelijke wil ik een overzicht van de vullingsgraad van elementen bij werkzoekendeprofielen
versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke  
**wil ik** een overzicht van de vullingsgraad van elementen bij werkzoekendeprofielen  
**zodat ik** 

Functioneel
-----------
De API om vragen te stellen bevat veel criteria. 

In eerste instantie volstaat een overzicht met daarop van elk criterium hoe vaak dat gevuld is. 
Hiermee wordt geprobeerd een eerste schifting op het belang van een criterium te krijgen.

Acceptatiecriteria
-------------------------
*Scenario : Opvragen overzicht gebruikte criteria*  
**Gegeven** 
**Wanneer** 
**Dan** 

[^loggingrecord] Voorstel overzicht

Aantal unieke profielen : 100

| Criterium | aantal | perc |
|-----------|--------|------|
| criteria1 | 11     | 11 % |
| criteria2 | 8      | 8%   |
| criteria3 | 0      | 0%   |
| criteria4 | 20     | 20%  |
