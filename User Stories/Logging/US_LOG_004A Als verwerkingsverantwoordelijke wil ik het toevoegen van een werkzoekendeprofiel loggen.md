# User story : US_LOG_004A Als verwerkingsverantwoordelijke wil ik het toevoegen van een werkzoekendeprofiel loggen zodat ik kan achterhalen wanneer gegevens toegevboegd zijn en ik voldoe aan de AVG

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | december 2022 | Initiele opzet |
| 0.10   | maart 2023    | Definitief     |
| 0.10   | maart 2023    | Gebruikte apparaat is komen te vervallen |

Functioneel
----------------
Binnen UM-W kunnen werkzoekendeprofielen via een upload of handmatig worden toegevoegd aan VUM. 
Omdat hierbij persoonsgegevens betrokken zijn moet de verwerking hiervan voldoen aan de AVG.

Technisch
-------------
Zie [US_LOG_004](US_LOG_004%20Als%20gegevensverantwoordelijke%20wil%20ik%20mutaties%20met%20betrekking%20tot%20een%20werkzoekende%20loggen.md)


Acceptatiecriteria
------------------------
**Gegeven** een gebruiker voert een werkzoekenprofiel in
**Wanneer** een nieuw werkzoekendeprofiel wordt opgeslagen
**Dan** is de gebeurtenis[^Gebeurtenis] en het werkzoekendeprofiel [^Profiel] gelogd

[^Gebeurtenis]
#### Gebeurtenis
- Vaste waarde : "Toevoegen van gegevens"
- Timestamp van het moment van toevoegen

#### Uitvoerder
- Uitvoerder (OIN van de uitvoerder, herkomst header)
- GebruikersId (herkomst header)
- ~~Gebruikte apparaat (herkomst header)~~

[^Profiel]
Zie [US_LOG_004](US_LOG_004%20Als%20gegevensverantwoordelijke%20wil%20ik%20mutaties%20met%20betrekking%20tot%20een%20werkzoekende%20loggen.md)
