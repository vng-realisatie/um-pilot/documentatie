# Feature : US_LOG_022 Als verwerkingsverantwoordelijke wil ik een overzicht het aantal hits per combinatie van criteria bij het zoeken naar profielen

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke  
**wil ik** een overzicht het aantal hits per combinatie van criteria bij het zoeken naar profielen  
**zodat ik** kan analyseren welke criteria succesvol zijn

Functioneel
-----------
De API om vragen te stellen bevat veel criteria. 
Door in kaart te brengen op welke criteria profielen gevonden worden, kan aan de bemiddelaars aangegeven worden wat de meest succesvolle wijze van filteren is.

In eerste instantie volstaat een overzicht met daarop van elk criterium hoe vaak dat gevuld is. 
Hiermee wordt geprobeerd een eerste schifting op het belang van een criterium te krijgen.

Acceptatiecriteria
-------------------------
*Scenario : Opvragen overzicht aantal hits per combinatie van criteria*  
**Gegeven**  
**Wanneer**  
**Dan**  

[^loggingrecord] Voorstel overzicht

| Criterium | aantal |
|-----------|--------|
| criteria1 | 11     |
| criteria2 | 8      |
| criteria3 | 0      |
| criteria4 | 20     |
