# Feature : US_LOG_011A Als verwerkingsverantwoordelijke wil ik de periodes op kunnen vragen wanneer een profiel in UM aanwezig is geweest

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke  
**wil ik** de periodes op kunnen vragen wanneer een profiel in UM aanwezig is geweest
**zodat ik** voldoe aan de BIO/AVG

Functioneel
----------------
Om te kunnen voldoen aan de Baseline Informatiebeveiliging Overheid (BIO) en daarmee aan de AVG wordt een aantal gegevens gelogd.
Deze gegevens moeten opvraagbaar zijn. Een van de gegevens die opgevraagd moeten kunnen worden betreft de periode waarin een profiel aanwezig was in UM.
Hiermee kan gecontroleerd worden wanneer een profiel is opgenomen en wanneer het is verwijderd.
Omdat UM werkt met het verwerken van batches (en geen individule mutaties doet van profielen) geldt dat een periode waarin het profiel aanwezig als volgt gedefinieerd is :
Een profiel is toegevoegd wanneer deze voor het eerst in een batch verschijnt tot het moment dat het profiel niet meer in een batch van hetzelfde OIN voorkomt.
Dit kan betekenen dat een profiel ook gedurende meerdere periodes aanwezig kan zijn.

Voor een uitgebreide achtergrond wordt hier verwezen naar de [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf) en de [AVG](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/avg-europese-privacywetgeving)

In [US_LOG_004](US_LOG_004A%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20het%20toevoegen%20van%20een%20werkzoekendeprofiel%20loggen.md) 
is beschreven hoe het toevoegen van profielen gelogd dienen te worden. Hierin is gesteld dat onderstaande gegevens beschikbaar moeten zijn : 

(a) de gebeurtenis (Vaste waarde : "Toevoegen van gegevens")
(b) GebruikersId (herkomst header)
(c) Uitvoerder (OIN van de uitvoerder, herkomst header)
(d) ~~Gebruikte apparaat (herkomst header)~~
(e) Timestamp van het moment van toevoegen
(f) het gehashte werkzoekendeId (wat mogelijk een BSN kan zijn)

Wanneer er een opdracht komt om deze gegevens te verstrekken dan dient UM obv het gehashte werkzoekendeId bovenstaande set gegevens te kunnen verstrekken.

Acceptatiecriteria
-------------------------
*Scenario : aanwezigheid van een specifiek profiel in UM*  
**Gegeven** een opdracht tot het inzichtelijk maken van de aanwezigheid van een specifiek profiel in UM
**Wanneer** het profiel aanwezig is geweest
**Dan** worden alle logregels van gebeurtenis "Toevoegen van gegevens" [^loggingrecord] overzichtelijk weergegeven 
**En** krijgen de logregels waarin het werkzoekendeId voorkomt de status toegevoegd
**En** krijgen de logregels waarin het werkzoekendeId niet voorkomt de status verwijderd

*Scenario : Profiel niet aanwezig in UM*  
**Gegeven** een opdracht tot het inzichtelijk maken van bevragingen van een specifiek profiel (gehasht werkzoekendeId)
**Wanneer** een opdracht tot het inzichtelijk maken van de aanwezigheid van een specifiek profiel in UM
**En** de gegevens worden opgevraagd
**Dan** levert de rapportage expliciet op dat er geen bevragingen (geslaagd of niet geslaagd) bekend zijn

[^loggingrecord] Voorstel overzicht

| datum        | tijdstip | werkzoekendeId (gehasht) | gebeurtenis             | GebruikersId | Uitvoerder OIN | status     |
|--------------|----------|--------------------------|-------------------------|--------------|----------------|------------|
| 3 maart 2023 | 11:00    | 123456789                | Toevoegen van gegevens  | username     | 987654321      | Toegevoegd |
| 5 maart 2023 | 08:01    | 123456789                | Toevoegen van gegevens  | username     | 987654321      | Toegevoegd |
| 6 maart 2023 | 20:10    | 123456789                | Toevoegen van gegevens  | username     | 987654321      | Verwijderd |
| 6 maart 2023 | 20:11    | 123456789                | Toevoegen van gegevens  | username     | 987654321      | Toegevoegd |

Mapping


| Functioneel            | Kibana veld                   | Veld value                    |
|------------------------|-------------------------------|-------------------------------|
| Datum / tijdstip       | request.creatieDatum          |                               |
| werkzoekendeId         | request.idWerkzoekende        |                               |
| Gebeurtenis            | requestSignature              | POST /werkzoekende/lijst{oin} |
| GebruikersId           | request.emailadres.emailadres |                               |
| Uitvoerder OIN         | fromOin                       |                               |
| status                 | Afhankelijk van aanwezigheid  |                               |
