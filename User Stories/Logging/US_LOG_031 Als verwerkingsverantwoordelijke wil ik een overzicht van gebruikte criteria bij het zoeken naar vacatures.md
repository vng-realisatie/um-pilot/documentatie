# Feature : US_LOG_031  Als verwerkingsverantwoordelijke wil ik een overzicht van gebruikte criteria bij het zoeken naar vacatures

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke  
**wil ik** een overzicht van gebruikte criteria bij het zoeken naar vacatures  
**zodat ik** kan sturen welke gegevens opgenomen worden in UM om de matching te verbeteren

Functioneel
-----------
De API om vragen te stellen bevat veel criteria. 
Door in kaart te brengen op welke criteria vacatures vooral gezocht worden, kan de vulling van de database met de gewenste gegevens gestuurd worden. 
Dit kan gebeuren door velden verplicht te gaan stellen, een prominentere plaats te geven of weg te halen (ruis).

In eerste instantie volstaat een overzicht met daarop van elk criterium hoe vaak dat gevuld is. 
Hiermee wordt geprobeerd een eerste schifting op het belang van een criterium te krijgen.

Acceptatiecriteria
-------------------------
*Scenario : Opvragen overzicht gebruikte criteria*  
**Gegeven** 
**Wanneer** 
**Dan** 

[^loggingrecord] Voorstel overzicht

| Criterium | aantal |
|-----------|--------|
| criteria1 | 11     |
| criteria2 | 8      |
| criteria3 | 0      |
| criteria4 | 20     |
