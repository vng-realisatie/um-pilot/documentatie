# User story :Als verwerkingsverantwoordelijke wil ik een poging tot verstrekken van gegevens van een inwoner loggen zodat ik voldoe aan de AVG

versie 0.10

_Versionering_

| versie | datum          | opmerking                                |
|--------|----------------|------------------------------------------|
| 0.10   | januari 2023   | Initiele opzet                           |
| 0.10   | maart 2023     | Gebruikte apparaat is komen te vervallen |
**Als** verwerkingsverantwoordelijke 
**wil ik** een poging tot verstrekken van gegevens van een inwoner loggen 
**zodat ik** voldoe aan de AVG

Functioneel
----------------

Om te kunnen voldoen aan de Baseline Informatiebeveiliging Overheid (BIO) en daarmee aan de AVG is het noodzakelijk om een aantal gegevens te loggen.
Voor een uitgebreide achtergrond wordt hier verwezen naar de [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf) en de [AVG](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/avg-europese-privacywetgeving)

Deze user story behelst de benodigde scenario's voor het niet succesvol bevragen van de werkzoekende bron.

Volgend uit de BIO dienen minimaal de volgende gegevens[^Logging] gelogd te worden :

Een logregel bevat minimaal:
(a) de gebeurtenis;
(b) de benodigde informatie die nodig is om het incident met hoge
mate van zekerheid te herleiden tot een natuurlijk persoon;
(c) ~~het gebruikte apparaat;~~
(d) het resultaat van de handeling;
(e) een datum en tijdstip van de gebeurtenis.

Aanvullend daarop is het noodzakelijk om het BSN (gehashed) mee te loggen, zodat bij navraag nagegaan kan worden wat er op basis van welke grondslag is opgevraagd.

[^Logging] Zie [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf)  sectie 12.4 Verslaglegging en monitoren

Technisch
--------------
De gegevens worden opgeslagen in een aparte stack : ELK (Elastic Search, Logstash, Kibana). 
Dit is tevens een aparte module [ELK](https://gitlab.com/vng-realisatie/um-pilot/elk)

Op verschillende plekken in de applicatie kan een module niet beschikbaar zijn of kan de benodigde authorisatie ontbreken. 
In deze gevallen moet de log verrijkt worden met gegevens omtrent deze bevraging.
Het afvangen van de authorisatie dient op zo'n hoog mogelijk niveau afgevangen te worden.
Dit niveau is afhankelijk van de wijze waarop de Gemeente VUM inzet

Deze plekken zijn minimaal :
- De bron zelf
- De API-gateway
- De Userinterface

Acceptatiecriteria
-------------------------
*Scenario : Bron niet bereikbaar*  
**Gegeven** een gebruiker heeft een vraag naar vacatures gesteld  
**Wanneer** de bron niet bereikbaar is   
**Dan** wordt een logregel[^loggingrecord] 'Vraag' aan de log toegevoegd
**En** heeft het resultaat de waarde 'UNAVAILABLE'

*Scenario : Gebruiker niet gautoriseerd*  
**Gegeven** een gebruiker heeft een vraag naar werkzoendendeprofielen gesteld  
**Wanneer** de gebruiker geen authorisatie daartoe heeft   
**Dan** worden een logregel[^loggingrecord] 'Detailvraag' aan de log toegevoegd
**En** heeft het resultaat de waarde 'UNAUTHORISED'

*Scenario : Bekijken detailgegevens werkzoekenden*  
**Gegeven** een gebruiker de detailsgegevens van een werkzoekendeprofiel opvraagt  
**Wanneer** de gebruiker geen authorisatie daartoe heeft   
**Dan** worden een logregel[^loggingrecord] 'Raadplegen' aan de log toegevoegd
**En** heeft het resultaat de waarde 'UNAUTHORISED'



[^loggingrecord]
#### Gebeurtenis
- Vaste waarde : "Verstrekking van gegevens : Vraag | Detailvraag | Raadplegen"
- Timestamp van het moment van bevraging

#### Aanvrager
- Uitvoerder (OIN van de uitvoerder, herkomst header)
- GebruikersId (herkomst header)
- ~~Gebruikte apparaat (herkomst header)~~

#### Resultaat
- UNAUTHORISED | UNAVAILABLE
- werkzoekendeId[] (gehashed BSN - Identificatie bevraagde inwoner)


