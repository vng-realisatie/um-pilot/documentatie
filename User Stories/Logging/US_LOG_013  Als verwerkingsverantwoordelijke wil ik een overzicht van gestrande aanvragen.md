# Feature : US_LOG_013  Als verwerkingsverantwoordelijke wil ik een overzicht van gestrande aanvragen

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke  
**wil ik** een overzicht van gestrande aanvragen  
**zodat ik** actie kan ondernemen om de werking van de applicatie te verbeteren  

Functioneel
-----------
Om de werking van de applicatie te kunnen monitoren dient de verwerkingsverantwoordelijke na te kunnen gaan of er bevragingen zijn geweest die niet verwerkt konden worden.
De verwerkingsverantwoordelijke wordt hiermee in staat gesteld de nodige actie te ondernemen.  

In [US_LOG_003A](US_LOG_003A%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20een%20poging%20tot%20verstrekken%20van%20gegevens%20van%20een%20inwoner%20loggen.md) 
is beschreven hoe de bevragingen gelogd dienen te worden. Hierin is gesteld dat onderstaande gegevens beschikbaar moeten zijn : 

(a) de gebeurtenis;
(b) de benodigde informatie die nodig is om het incident met hoge
mate van zekerheid te herleiden tot een natuurlijk persoon (het gehashte werkzoekendeId (wat mogelijk een BSN kan zijn))
(c) het gebruikte apparaat;
(d) het resultaat van de handeling;
(e) een datum en tijdstip van de gebeurtenis.

Wanneer er een opdracht komt om deze gegevens te verstrekken dan dient UM bovenstaande set gegevens te kunnen verstrekken.

Acceptatiecriteria
-------------------------
*Scenario : Gestrande bevragingen*  
**Gegeven** een opdracht tot het inzichtelijk maken van gestrande bevragingen  
**Wanneer** er een of meerdere gefaalde bevragingen bekend zijn  
**Dan** worden de logregels[^loggingrecord] overzichtelijk weergegeven  

*Scenario : Geen gestrande bevragingen*  
**Gegeven** een opdracht tot het inzichtelijk maken van gestrande bevragingen  
**Wanneer** er geen gestrande bevragingen bekend zijn  
**En** de gegevens worden opgevraagd  
**Dan** levert de rapportage expliciet op dat er geen gestrande bevragingen bekend zijn 

[^loggingrecord] Voorstel overzicht

| datum        | tijdstip | Type         | id        | gebeurtenis | GebruikersId | Uitvoerder OIN | resultaat   |
|--------------|----------|--------------|-----------|-------------|--------------|----------------|-------------|
| 3 maart 2023 | 11:00    | Werkzoekende | 123456789 | Vraag       | username     | 987654321      | UNAVAILABLE |
| 5 maart 2023 | 08:01    | Vacature     | 123456789 | Detailvraag | username     | 987654321      | UNAVAILABLE |
| 6 maart 2023 | 20:10    | Werkzoekende | 123456789 | Raadplegen  | username     | 987654321      | UNAVAILABLE |
| 6 maart 2023 | 20:11    | Werkzoekende | 123456789 | Raadplegen  | username     | 987654321      | UNAVAILABLE |

Mapping

| Functioneel      | Kibana veld                   | Veld value                      |
|------------------|-------------------------------|---------------------------------|
| Datum / tijdstip | request.creatieDatum          |                                 |
| Type             | requestSignature              | Afleidbaar uit requestSignature |
| id               |                               |                                 |
| * Vacature       | request.idWerkzoekende        |                                 |
| * Werkzoekende   | request.idVacature            |                                 |
| Gebeurtenis      | requestSignature              |                                 |
| GebruikersId     | request.emailadres.emailadres |                                 |
| Uitvoerder OIN   | fromOin                       |                                 |
| resultaat        | ?                             | 400                             |
