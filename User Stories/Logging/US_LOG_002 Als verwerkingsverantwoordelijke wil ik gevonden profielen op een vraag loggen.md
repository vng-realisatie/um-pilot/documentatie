# User story : Als verwerkingsverantwoordelijke wil ik gevonden profielen op een vraag loggen zodat ik voldoe aan de AVG

versie 0.10

_Versionering_

| versie | datum         | opmerking        |
|--------|---------------|------------------|
| 0.10   | december 2022 | Initiele opzet   |
| 0.10   | maart 2023    | Definitief       |



Functioneel
----------------
Deze logging dient als doel om zoekprocessen te kunnen optimaliseren. 
De logging dient derhalve niet om aan de AVG te voldoen. 
Om buiten het regime van de AVG te blijven worden in de log geen contactgegevens van de inwoner opgeslagen of andere gegevens die terug kunnen leiden op een inwoner.

Technisch
-------------
De gegevens worden opgeslagen in een aparte stack : ELK (Elastic Search, Logstash, Kibana). Dit is tevens een aparte module [ELK](https://gitlab.com/vng-realisatie/um-pilot/elk)

Acceptatiecriteria
-------------------------
**Gegeven** een gebruiker gaat profielen op vacatures matchen  
**Wanneer** de gebruiker de profielen opvraagt  
**Dan** wordt voor de vraag per profiel een logregel[^Profiel] aan de log toegevoegd

[^Referentie]

#### Log
- vraagId
- werkzoekendeIds[werkzoekendeId]
- timeStamp
