# Feature :  Als verwerkingsverantwoordelijke wil ik verwerkingen loggen zodat er voldaan wordt aan de AVG

versie 0.10

_Versionering_

| versie | datum        | opmerking                             |
|--------|--------------|---------------------------------------|
| 0.10   | januari 2023 | Initiele opzet                        |


Functioneel
----------------
De loggingprincipes waaraan voldaan moet worden staan beschreven in de avg & common ground [logging principes](https://commonground.nl/cms/view/c99f1789-adba-4fa7-bc2d-e1f8a96618c5/api-standaarden-voor-logging-en-verwerking)

Onder het loggen worden twee subdomeinen onderscheiden, te weten :
- de verwerking van persoonsgegevens kunnen vastleggen in een verwerkingenlog door de processapplicatie
- het ontsluiten van de verwerkingenlog naar de inwoner en andere geautoriseerde partijen

In de DPIA is nader toegelicht wat er gelogd moet worden :

*Bouwsteen “Logging gebruikers"*  
Telkens als er een inlogactie plaats vindt – E-Herkenning of PKI-Certificaat – dan wordt dit gelogd samen met de service waarbij wordt ingelogd. Bij deze logging wordt ook Datum/tijd vastgelegd.
Foutief inlog wordt ook met datum/tijd vastgelegd
Specifieke acties kunnen worden gelogd waarbij de actie, certificaatgegevens en datum/tijd worden vastgelegd.


*Bouwsteen “Logging-Raadpleging”*

Speciale aandacht gaat uit naar het raadplegen van profielen. Als een profiel wordt geraadpleegd dan moet worden vastgelegd wie, wat, wanneer heeft geraadpleegd (AVG).  Elke keer als een profiel wordt aangeboden dienen bovenstaande gegevens te worden vastgelegd.

- Wie is de uiteindelijke bemiddelaar aan wie de gegevens worden verstrekt
- Wat zijn de gegevens in het profiel die worden verstrekt
- Wanneer is het tijdstip datum & tijd dat de verstrekking heeft plaatsgevonden

Voor deze bouwsteen gebruiken we de API zoals beschreven staat in [Gemma online.](https://www.gemmaonline.nl/index.php/Interactiepatronen_Verwerkingenlogging )

De API-documentatie voor deze bouwsteen is beschreven in [Github] (https://vng-realisatie.github.io/gemma-verwerkingenlogging/)

Dit resulteert in de volgende User stories :

[US_LOG_001](US_LOG_001%20%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20vragen%20over%20werkzoekenden%20loggen.md)
[US_LOG_002](US_LOG_002%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20gevonden%20profielen%20op%20een%20vraag%20loggen.md)
[US_LOG_003](US_LOG_003%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20het%20verstrekken%20van%20gegevens%20van%20een%20inwoner%20loggen.md)
[US_LOG_004](US_LOG_004%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20mutaties%20met%20betrekking%20tot%20een%20werkzoekende%20loggen.md)