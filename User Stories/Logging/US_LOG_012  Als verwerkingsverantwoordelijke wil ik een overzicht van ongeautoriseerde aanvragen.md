# Feature : US_LOG_012  Als verwerkingsverantwoordelijke wil ik een overzicht van ongeautoriseerde aanvragen
versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke
**wil ik** een overzicht van ongeautoriseerde aanvragen
**zodat ik** actie kan ondernemen om misbruik tegen te gaan

Functioneel
----------------
Om de veiligheid van de applicatie te kunnen waarborgen dient de verwerkingsverantwoordelijke na te kunnen gaan of er ongeautoriseerde bevragingen worden gedaan.
De verwerkingsverantwoordelijke wordt daarmee in staat gesteld de nodige actie te ondernemen.  

In[US_LOG_003A](US_LOG_003A%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20een%20poging%20tot%20verstrekken%20van%20gegevens%20van%20een%20inwoner%20loggen.md) 
is beschreven hoe de bevragingen gelogd dienen te worden. Hierin is gesteld dat onderstaande gegevens beschikbaar moeten zijn : 

(a) de gebeurtenis;
(b) de benodigde informatie die nodig is om het incident met hoge
mate van zekerheid te herleiden tot een natuurlijk persoon (het gehashte werkzoekendeId (wat mogelijk een BSN kan zijn))
(c) ~~het gebruikte apparaat;~~
(d) het resultaat van de handeling;
(e) een datum en tijdstip van de gebeurtenis.

Wanneer er een opdracht komt om deze gegevens te verstrekken dan dient UM bovenstaande set gegevens te kunnen leveren.

Acceptatiecriteria
-------------------------
*Scenario : Ongeautoriseerde bevragingen*  
**Gegeven** een opdracht tot het inzichtelijk maken van ongeautoriseerde bevragingen 
**Wanneer** er een of meerdere ongeautoriseerde bevragingen bekend zijn
**Dan** worden de logregels[^loggingrecord] overzichtelijk weergegeven 

*Scenario : Geen ongeautoriseerde bevragingen*  
**Gegeven** een opdracht tot het inzichtelijk maken van ongeautoriseerde bevragingen
**Wanneer** er geen ongeautoriseerde bevragingen bekend zijn
**En** de gegevens worden opgevraagd
**Dan** levert de rapportage expliciet op dat er geen ongeautoriseerde bevragingen bekend zijn

[^loggingrecord] Voorstel overzicht

| datum        | tijdstip | Type         | id        | gebeurtenis | GebruikersId | Uitvoerder OIN | resultaat     |
|--------------|----------|--------------|-----------|-------------|--------------|----------------|---------------|
| 3 maart 2023 | 11:00    | Werkzoekende | 123456789 | Vraag       | username     | 987654321      | UNAUTHORISED  |
| 5 maart 2023 | 08:01    | Vacature     | 123456789 | Detailvraag | username     | 987654321      | UNAUTHORISED  |
| 6 maart 2023 | 20:10    | Werkzoekende | 123456789 | Raadplegen  | username     | 987654321      | UNAUTHORISED  |
| 6 maart 2023 | 20:11    | Werkzoekende | 123456789 | Raadplegen  | username     | 987654321      | UNAUTHORISED  |

Mapping

| Functioneel      | Kibana veld                   | Veld value                      |
|------------------|-------------------------------|---------------------------------|
| Datum / tijdstip | request.creatieDatum          |                                 |
| Type             | requestSignature              | Afleidbaar uit requestSignature |
| id               |                               |                                 |
| * Vacature       | request.idWerkzoekende        |                                 |
| * Werkzoekende   | request.idVacature            |                                 |
| Gebeurtenis      | requestSignature              |                                 |
| GebruikersId     | request.emailadres.emailadres |                                 |
| Uitvoerder OIN   | fromOin                       |                                 |
| resultaat        | ?                             | 403                             |