# Feature : Als verwerkingsverantwoordelijke wil ik vragen over werkzoekenden loggen zodat ik het matchingsproces kan verbeteren

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | december 2022 | Initiele opzet |
| 0.10   | maart 2023    | Definitief     |


Functioneel
----------------
Deze logging dient als doel om zoekprocessen te kunnen optimaliseren. Hiertoe wordt de vraag om profielen gelogd.

Technisch
-------------
De gegevens worden opgeslagen in een aparte stack : ELK (Elastic Search, Logstash, Kibana). Dit is tevens een aparte module [ELK](https://gitlab.com/vng-realisatie/um-pilot/elk)

Acceptatiecriteria
-------------------------
**Gegeven** een gebruiker gaat profielen op vacatures matchen  
**Wanneer** de gebruiker de profielen opvraagt  
**Dan** wordt voor de vraag een logregel[^Vraag] aan de log toegevoegd

[^Vraag]

#### Algemene gegevens
- VraagId
- Zoektitel
- Indicatie LDR registratie
- Indicatie beschikbaarheid contactgegevens

#### Flexibiliteit
- Code regiostraal
- Indicatie onregelmatig werk of ploegendienst
- Datum aanvang beschikbaar voor werk
- Datum einde beschikbaar voor werk
- Code werk en denk niveau werkzoekende

#### Werktijden
- Aantal werkuren per week minimaal
- Aantal werkuren per week maximaal
- Indicatie kantoortijden

#### Mobiliteit
- Bemiddelingspostcode
- Maximale reisafstand  (km)
- Maximale reistijd (min)

#### Vakvaardigheid
- Omschrijving vakvaardigheid

#### Gedragscompetentie
- Gedragscompetentie
- Beheersing gedragscompetentie
- Omschrijving gedragscompetentie

#### Rijbewijs
- Rijbewijs code

#### Cursus
- Naam cursus
- Datum behalen certificaat

#### Opleiding
- Niveau Opleiding
- Type opleidingsnaam
- Diploma behaald
- Datum behalen diploma
- Code opleidingsnaam
- Omschrijving opleiding

#### Taalbeheersing
- Code taal
- Taalbeheersing mondeling
- Taalbeheersing schriftelijk
- Taalbeheersing lezen
- Taalbeheersing luisteren

#### Werkervaring
- Datum aanvang werkzaamheden
- Datum einde werkzaamheden
- Type beroepsnaam
- Naam organisatie

#### Vervoermiddel
- Vervoermiddel beschikbaar voor uitvoering werk
- Vervoermiddel beschikbaar voor woon-werkverkeer

#### Voorkeursland
- Naam voorkeursland

#### Contractvorm
- Type arbeidscontract
- Type overeenkomst

#### Sector
- Sector beroeps en bedrijfsleven

#### Bemiddelingsberoep
- Type beroepsnaam