# Feature : US_LOG_011  Als verwerkingsverantwoordelijke wil ik op kunnen vragen wanneer een profiel bevraagd is

versie 0.10

_Versionering_

| versie | datum         | opmerking      |
|--------|---------------|----------------|
| 0.10   | maart 2023    | Initiele opzet |

**Als** verwerkingsverantwoordelijke
**wil ik** op kunnen vragen wanneer een profiel bevraagd is
**zodat ik** voldoe aan de BIO/AVG

Functioneel
----------------
Om te kunnen voldoen aan de Baseline Informatiebeveiliging Overheid (BIO) en daarmee aan de AVG is het noodzakelijk om een aantal gegevens te loggen.
Voor een uitgebreide achtergrond wordt hier verwezen naar de [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf) en de [AVG](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/avg-europese-privacywetgeving)

Deze user story behelst de benodigde scenario's voor het opvragen van profielgegevens uit de werkzoekende bron.

In [US_LOG_003](US_LOG_003%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20het%20verstrekken%20van%20gegevens%20van%20een%20inwoner%20loggen.md) en 
[US_LOG_003A](US_LOG_003A%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20een%20poging%20tot%20verstrekken%20van%20gegevens%20van%20een%20inwoner%20loggen.md) 
is beschreven hoe de bevragingen gelogd dienen te worden. Hierin is gesteld dat onderstaande gegevens beschikbaar moeten zijn : 

(a) de gebeurtenis;
(b) de benodigde informatie die nodig is om het incident met hoge
mate van zekerheid te herleiden tot een natuurlijk persoon (het gehashte werkzoekendeId (wat mogelijk een BSN kan zijn))
(c) ~~het gebruikte apparaat;~~
(d) het resultaat van de handeling;
(e) een datum en tijdstip van de gebeurtenis.

Wanneer er een opdracht komt om deze gegevens te verstrekken dan dient UM obv het gehashte werkzoekendeId bovenstaande set gegevens te kunnen verstrekken.

Acceptatiecriteria
-------------------------
*Scenario : Profiel bevraagd*  
**Gegeven** een opdracht tot het inzichtelijk maken van bevragingen van een specifiek profiel (gehasht werkzoekendeId)
**Wanneer** er een of meerdere bevragingen bekend zijn
**Dan** worden de logregels[^loggingrecord] overzichtelijk weergegeven 

*Scenario : Profiel niet bevraagd*  
**Gegeven** een opdracht tot het inzichtelijk maken van bevragingen van een specifiek profiel (gehasht werkzoekendeId)
**Wanneer** er geen bevragingen bekend zijn
**En** de gegevens worden opgevraagd
**Dan** levert de rapportage expliciet op dat er geen bevragingen (geslaagd of niet geslaagd) bekend zijn

[^loggingrecord] Voorstel overzicht

| datum        | tijdstip | werkzoekendeId (gehasht) | gebeurtenis | GebruikersId | Uitvoerder OIN | resultaat    |
|--------------|----------|--------------------------|-------------|--------------|----------------|--------------|
| 3 maart 2023 | 11:00    | 123456789                | Vraag       | username     | 987654321      | OK           |
| 5 maart 2023 | 08:01    | 123456789                | Detailvraag | username     | 987654321      | UNAVAILABLE  |
| 6 maart 2023 | 20:10    | 123456789                | Raadplegen  | username     | 987654321      | UNAUTHORISED |
| 6 maart 2023 | 20:11    | 123456789                | Raadplegen  | username     | 987654321      | OK           |


Mapping

| Functioneel      | Kibana veld                   | Kibana Veld value                           |
|------------------|-------------------------------|---------------------------------------------|
| Datum / tijdstip | request.creatieDatum          |                                             |
| werkzoekendeId   | request.idWerkzoekende        |                                             |
| Gebeurtenis      | requestSignature              |                                             |
| * Vraag          |                               | POST /werkzoekendeProfielen/matches         | 
| * Detailvraag    |                               | GET /werkzoekendeProfielen/{idWerkzoekende} | 
| * Raadplegen     |                               | GET /werkzoekende/lijst{oin}                | 
| GebruikersId     | request.emailadres.emailadres |                                             |
| Uitvoerder OIN   | fromOin                       |                                             |
| status           | ?                             |                                             |
| * OK             |                               | 200                                         |
| * UNAVAILABLE    | 400                           | 400                                         |
| * UNAUTHORISED   | 403                           | 403                                         |