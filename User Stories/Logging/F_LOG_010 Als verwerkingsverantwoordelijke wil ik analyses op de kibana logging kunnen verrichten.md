# Feature :  F_LOG_010 Als verwerkingsverantwoordelijke wil ik analyses op de kibana logging kunnen verrichten

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |


Functioneel
----------------
Om UM verder te kunnen ontwikkelen en beter af te laten stemmen op het gebruik door en de wensen van de gebruikers 
is het noodzakelijk de log met bevragingen te kunnen analyseren.
Deze analyse moeten antwoord kunnen geven op vragen als "op welke velden worden de meeste bevragingen gedaan", "welke velden worden niet gebruikt", "welke bevragingen leveren de meeste hits op".  

Naast deze functionele voorziening moeten uit de logging ook gegevens gehaald kunnen worden met betrekking tot de BIO. 
Het gaat hierom vragen als "wanneer is een BSN bevraagd", "welke gegevens zijn gedeeld", etc. 

De benodigde analyses en bevragingen worden nader uitgewerkt in onderstaande userstories

- [US_LOG_011](US_LOG_011%20%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20op%20kunnen%20vragen%20wanneer%20een%20profiel%20bevraagd%20is.md)
- [US_LOG_011A](US_LOG_011A%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20de%20periodes%20op%20kunnen%20vragen%20wanneer%20een%20profiel%20in%20UM%20aanwezig%20is%20geweest.md)
- [US_LOG_012](US_LOG_012%20%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20een%20overzicht%20van%20ongeautoriseerde%20aanvragen.md)
- [US_LOG_013](US_LOG_013%20%20Als%20verwerkingsverantwoordelijke%20wil%20ik%20een%20overzicht%20van%20gestrande%20aanvragen.md)

