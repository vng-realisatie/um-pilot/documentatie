# User story : Als verwerkingsverantwoordelijke wil ik het verstrekken van gegevens van een inwoner loggen zodat ik voldoe aan de AVG

versie 0.10

_Versionering_

| versie | datum         | opmerking                                |
|--------|---------------|------------------------------------------|
| 0.10   | december 2022 | Initiele opzet                           |
| 0.10   | maart 2023    | Gebruikte apparaat is komen te vervallen |


Functioneel
----------------
Om te kunnen voldoen aan de Baseline Informatiebeveiliging Overheid (BIO) en daarmee aan de AVG is het noodzakelijk om een aantal gegevens te loggen. Voor een uitgebreide achtergrond wordt hier verwezen naar de [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf) en de [AVG](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/avg-europese-privacywetgeving)

Volgend uit de BIO dienen minimaal de volgende gegevens[^Logging] gelogd te worden :

Een logregel bevat minimaal:
(a) de gebeurtenis;
(b) de benodigde informatie die nodig is om het incident met hoge
mate van zekerheid te herleiden tot een natuurlijk persoon;
(c) ~~het gebruikte apparaat;~~
(d) het resultaat van de handeling;
(e) een datum en tijdstip van de gebeurtenis.

Aanvullend daarop is het noodzakelijk om het BSN (gehashed) mee te loggen, zodat bij navraag nagegaan kan worden wat er op basis van welke grondslag is opgevraagd.

[^Logging] Zie [BIO](https://bio-overheid.nl/media/1572/bio-versie-104zv_def.pdf)  sectie 12.4 Verslaglegging en monitoren

Technisch
--------------
De gegevens worden opgeslagen in een aparte stack : ELK (Elastic Search, Logstash, Kibana). Dit is tevens een aparte module [ELK](https://gitlab.com/vng-realisatie/um-pilot/elk)

Acceptatiecriteria
-------------------------
*Scenario : Matching vacature werkzoekende*  
**Gegeven** een gebruiker heeft een vraag naar vacatures gesteld  
**Wanneer** er werkzoekende profielen zijn gevonden  
**Dan** wordt een logregel[^loggingrecord] 'Vraag' aan de log toegevoegd

*Scenario : Bekijken detailgegevens matching*  
**Gegeven** een gebruiker heeft profielen op vacatures gevonden  
**Wanneer** de gebruiker de details van een profiel opvraagt  
**Dan** worden een logregel[^loggingrecord] 'Detailvraag' aan de log toegevoegd

*Scenario : Bekijken detailgegevens werkzoekenden*  
**Gegeven** een gebruiker heeft het een overzicht van werkzoekendeprofielen  
**Wanneer** de gebruiker de details van een werkzoekendeprofielen opvraagt  
**Dan** worden een logregel[^loggingrecord] 'Raadplegen' aan de log toegevoegd



[^loggingrecord]
#### Gebeurtenis
- Vaste waarde : "Verstrekking van gegevens : Vraag | Detailvraag | Raadplegen"
- Timestamp van het moment van bevraging

#### Aanvrager
- Uitvoerder (OIN van de uitvoerder, herkomst header)
- GebruikersId (herkomst header)
- ~~Gebruikte apparaat (herkomst header)~~

#### Resultaat
- OK
- werkzoekendeId[] (gehashed BSN - Identificatie bevraagde inwoner)


