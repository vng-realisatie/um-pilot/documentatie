# User stories

versie 0.8

_Versionering_  

| versie | datum          | opmerking                             |
|--------|----------------|---------------------------------------|
| 0.1    | september 2022 | Initiele opzet                        |
| 0.8    | oktober 2022   | Werkzoekende en vacature user stories |


## 1.1 Doel

Dit document beschrijft verschillende userstories met behulp van ondersteunende sequence diagrammen van de onderliggende componenten en interacties/api calls, en verbindt daarmee het functionele en technische perspectief op de UM appliatie.

## 1.2 Overzicht
Hieronder volgt een overzicht van de user stories en tevens een indicatie onder welk menu item deze user stories start. 

| User stories per menu item                                                                                                                                                                                                                                                                                                     |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![menu-vs-userstories.png](userstories-images/menu-vs-userstories.png "User stories geplot op menu in UM portaal")                                                                                                                                                                                                             |
| #1 Als gebruiker inloggen op het medewerkersportaal (geen menu item)                                                                                                                                                                                                                                                           |
| #2 Als systeem van een gebruiker direct inloggen op de API gateway van UM (geen menu item)                                                                                                                                                                                                                                     |
| #3 Als Bemiddelaar stel ik een vraag om op basis van kenmerken werkzoekenden te zoeken                                                                                                                                                                                                                                         |
| Als Bemiddelaar zoekvragen en matchingprofielen raadplegen<br/> #4 Als Bemiddelaar wil ik zoekvragen raadplegen<br/>  #5 Als Bemiddelaar wil ik zoekvraag resultaten raadplegen<br/>  #6 Als Bemiddelaar wil ik matchingsprofielen raadplegen<br/>  #7 Als Bemiddelaar wil ik details van een matchingsprofiel raadplegen<br/> |
| Als Gemeentemedewerker Werkzoekende Bron vullen en raadplegen<br/> #8 Als Gemeentemedewerker wil ik werkzoekende profielen uploaden<br/> #9 Als Gemeentemedewerker wil ik werkzoekende overzicht raadplegen<br/> #10 Als Gemeentemedewerker wil ik details van een werkzoekende raadplegen                                     |
| #11 Als Bemiddelaar stel ik een vraag om op basis van kenmerken vacatures te zoeken                                                                                                                                                                                                                                            |
| Als Bemiddelaar zoekvragen en vacatures raadplegen<br/> #12 Als Bemiddelaar wil ik zoekvragen raadplegen<br/>  #13 Als Bemiddelaar wil ik zoekvraag resultaten raadplegen<br/>  #14 Als Bemiddelaar wil ik vacatures raadplegen<br/>  #15 Als Bemiddelaar wil ik details van een vacature raadplegen<br/>                      |
| Als Gemeentemedewerker Vacature Bron vullen en raadplegen<br/> #16 Als Gemeentemedewerker wil ik vactures uploaden<br/> #17 Als Gemeentemedewerker wil ik het vacature overzicht raadplegen<br/> #18 Als Gemeentemedewerker wil ik details van een vacature raadplegen                                                         |
  

Een aantal user stories hebben een volgordelijke afhankelijkheid, en zijn daarom gegroepeerd.


## 1.3 Uitwerking per User Story

<details open><summary>#1 Als gebruiker inloggen op het medewerkersportaal</summary><p>

| Sequence Diagram                                                                                      
|:-------------------------------------------------------------------------------------------------------:|
| ![userstory1.png](userstories-images/userstory1.png "Als gebruiker inloggen op het medewerkersportaal") |

| interactie | wie/wat                                        | toelichting                                                                                                                                                  | roept API  | input                                                     | output                |
|------------|------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----------------------------------------------------------|-----------------------|
| 1.0        | Gebruiker                                      | logt in op het medewerkersportaal. De Identity Server Keycloak levert als antwoord een accesstoken terug. Het access token bevat de rollen van de gebruiker. | UM-SEC1-v1 | gebruikersnaam, gebruikerswachtwoord, front end client id | access token          |
| 1.1        | Gebruiker                                      | wordt met een geldig accesstoken geredirect naar het hoofdmenu van het portal.                                                                               |            | access token                                              | opgevraagde webpagina |
| 1.2        | Medewerkersportaal                             | roept een API aan om informatie op te halen of over te brengen                                                                                               |            | access token                                              |                       |
| 1.2.1      | API Gateway                                    | `Indien access token nog niet bekend is bij API Gateway dan wordt bij Keycloak het token gevalideerd`                                                        |            | access token                                              | ok/nok                |
| 1.2.2      | API Gateway                                    | `Bij een 'OK' routeert Kong de API call naar de in Kong geconfigureerde API`                                                                                 |            |                                                           |                       |
| 1.3        | : Upstream API, bijvoorbeeld de vacatures-bron | Verifieerd de access token bij de Identity Server                                                                                                            |            | access token                                              | ok/nok                |

 *  Het 'front end' client id verwijst naar een client geconfigureerd in Keycloak ten behoeve van het medewerkersportaal.
 *  Het access token bevat de gebruikersrollen
 *  Gebruikersnaam en wachtwoord zijn de user credentials van de gemeentemedewerker geconfigureerd in Keycloak
 *  Het 'back end' client id+secret verwijst naar een client geconfigureerd in Keycloak ten behoeve van back end applicaties zoals Kong.
  
</p></details>

<details><summary>#2 Als systeem van een gebruiker direct inloggen op de API gateway van UM</summary><p>


  |Sequence Diagram|
  |:--:|
  |![userstory2.png](userstories-images/userstory2.png "Als systeem van een gebruiker direct inloggen op de API gateway van UM")|

| interactie | API id     | toelichting                                                                               |
|------------|------------|-------------------------------------------------------------------------------------------|
| 2.0        | UM-SEC2-v1 | Het systeem haalt op basis van credentials een accesstoken op bij de AIM server Keycloak. |
| 2.1        |            | Het systeem roept de API’s aan op de API gateway met gebruik van het geldige accesstoken. |

</p></details>

<details><summary>#3 Als Bemiddelaar stel ik een vraag om op basis van kenmerken werkzoekenden te zoeken</summary><p>


| Sequence Diagram                                                                                     |
|:-------------------------------------------------------------------------------------------------------:|
| ![userstory3.png](userstories-images/userstory3.png "Als Bemiddelaar stel ik een vraag om op basis van kenmerken werkzoekenden te zoeken") |


| interactie | wie/wat                          | toelichting                                                                                                                                                                        | roept API      | input                            | output                           |
|------------|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|----------------------------------|----------------------------------|
| 3.0        | Bemiddelaar                      | kiest de menukeuze ”Vraag werkzoekenden aan” om een vraag te kunnen stellen.                                                                                                       | n/a            |                                  |                                  |
| 3.1        | Medewerkersportaal               | stuurt de vraag naar de API Gateway.                                                                                                                                               | UM-BMW2-v1 gw  | zoekcriteria                     | ok/nok                           |
| 3.1.1      | API Gateway                      | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.`                                                                                        |                |                                  | ok/nok                           |
| 3.1.2      | API Gateway                      | `routeert de vraag naar Werkzoekende Bemiddelaar Service`                                                                                                                          | UM-BMW2-v1     | zoekcriteria                     | ok/nok                           |
| 3.1.3      | Werkzoekende Bemiddelaar Service | `Als je gemachtigd bent, wordt de vraag opgeslagen in de database van de Werkzoekende Bemiddelaar Service`                                                                         |                | zoekcriteria                     | ok/nok                           |
| 3.2        | Werkzoekende Bemiddelaar Service | zet de vraag uit via de API gateway.                                                                                                                                               | VUM-BRW1-v1 gw | zoekcriteria                     | ok/nok                           |
| 3.2.1      | API Gateway                      | `stuurt de vraag door naar VUM.`                                                                                                                                                   | VUM-BRW1-v1    |                                  |                                  |
| 3.3        | VUM                              | zet de vraag uit naar de aangesloten bronnen op basis van kriteria waaronder “bemiddelspostcode”. De vraag komt binnen op de Reverse Proxy van de Bron.                            | VUM-BRW1-v1 gw | zoekcriteria                     | ok/nok                           |
| 3.3.1      | Reverse Proxy                    | `haalt de nodige informatie uit het door VUM meegeleverde Client Certificaat en zet deze informatie in de HTTP-Header. De Reverse Proxy zet het verzoek door naar de API Gateway.` | VUM-BRW1-v1 gw | zoekcriteria                     | matchende  werkzoekendeprofielen |
| 3.3.2      | API Gateway                      | `De API Gateway zet het verzoek door naar de Werkzoekende Bron service`                                                                                                            | VUM-BRW1-v1    | zoekcriteria                     | matchende  werkzoekendeprofielen |
| 3.4        | VUM                              | Stuurt de lijst van matchende werkzoekende profielen door                                                                                                                          | VUM-BMW3-v1 gw | matchende  werkzoekendeprofielen |                                  |
| 3.4.1      | API gateway                      | `Doorsturen van de VUM bericht`                                                                                                                                                    | VUM-BMW3 v1    | matchende  werkzoekendeprofielen |                                  |
| 3.4.2      | Werkzoekende Bemiddelaar Service | `De matchende profielen worden opgeslagen in de database van de Werkzoekende Bemiddelaar Service bij de oorspronkelijke vraag.`                                                    |                |                                  |                                  |
 
### schermafbeeldingen van VNG portaal toen "vraag werkzoekende aan" optie wordt gebruikt
![Screenshot1.png](screenshots van VUM/vraag werkzoekende/Screenshot1.png)
![Screenshot 2.png](screenshots van VUM/vraag werkzoekende/Screenshot 2.png)
![Screenshot 3.png](screenshots van VUM/vraag werkzoekende/Screenshot 3.png)
![Screenshot 4.png](screenshots van VUM/vraag werkzoekende/Screenshot 4.png)

### Terug kijken naar de aanvraag voor werkzoekende gaat via "opgeslagen zoekvraag werkzoekenden
![Screenshot 5.png](screenshots van VUM/vraag werkzoekende/Screenshot 5.png)


</p></details>


<details><summary>Als Bemiddelaar zoekvragen en matchingprofielen raadplegen<br/>  
#4 Als Bemiddelaar wil ik zoekvragen raadplegen<br/>  
#5 Als Bemiddelaar wil ik zoekvraag resultaten raadplegen<br/>  
#6 Als Bemiddelaar wil ik matchingsprofielen raadplegen<br/>  
#7 Als Bemiddelaar wil ik details van een matchingsprofiel raadplegen<br/>  
</summary><p>

  |Sequence Diagram|
  |:--:|
  |![userstory4-7.png](userstories-images/userstory4-7.png "Raadplegen")|

| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output                                     |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------------------------------------------|
| 4.0        | Bemiddelaar        | kiest de menukeuze ”Opgeslagen zoekvraag werkzoekenden”                                     | n/a           |       |                                            |
| 4.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMW1-v1 gw | oin   | vraag id, vumId, zoekvragen, werkzoekenden |
| 4.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok                                     |
| 4.1.2      | API Gateway        | `routeert de vraag naar Werkzoekende Bemiddelaar Service`                                   | UM-BMW1-v1    | oin   | vraag id, vumId, zoekvragen, werkzoekenden |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input    | output                                     |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|----------|--------------------------------------------|
| 5.0        | Bemiddelaar        | klikt op 'Bekijk Werkzoekenden'                                                             | n/a           |          |                                            |
| 5.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMW1-v1 gw | vraag id | vraag id, vumId, zoekvragen, werkzoekenden |
| 5.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |          | ok/nok                                     |
| 5.1.2      | API Gateway        | `routeert de vraag naar Werkzoekende Bemiddelaar Service`                                   | UM-BMW1-v1    | vraag id | vraag id, vumId, zoekvragen, werkzoekenden |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input       | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------------|--------|
| 6.0        | Bemiddelaar        | klikt op 'Meer detail'                                                                      | n/a           |             |        |
| 6.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMW3-v1 gw | oin vraagId |        |
| 6.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |             | ok/nok |
| 6.1.2      | API Gateway        | `routeert de vraag naar Werkzoekende Bemiddelaar Service`                                   | UM-BMW3-v1    |             |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input             | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------------------|--------|
| 7.0        | Bemiddelaar        | klikt op 'Vraag detail werkzoekende aan'                                                    | n/a           |                   |        |
| 7.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMW4-v1 gw | oin vraagId vumId |        |
| 7.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |                   | ok/nok |
| 7.1.2      | API Gateway        | `routeert de vraag naar VUM`                                                                |               | oin vraagId vumId |        |
| 7.1.3      | Reverse proxy      | `routeert de vraag naar VUM`                                                                |               |                   |        |
| 7.1.4      | API Gateway        | `routeert de vraag naar Werkzoekende bron service`                                          |               |                   |        |

</p></details>

<details><summary>Als Gemeentemedewerker Werkzoekende bron vullen en raadplegen<br/>  
#8 Als Gemeentemedewerker wil ik werkzoekende profielen uploaden<br/>  
#9 Als Gemeentemedewerker wil ik werkzoekende overzicht raadplegen<br/>  
#10 Als Gemeentemedewerker wil ik details van een werkzoekende raadplegen<br/>  
</summary><p>

  |Sequence Diagram|
  |:--:|
  |![userstory8-10.png](userstories-images/userstory8-10.png "Werkzoekende bron")|


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------|
| 8.0        | Gemeentemedewerker | upload file via menukeuze 'Upload werkzoekenden'                                            | n/a           |       |        |
| 8.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRW1-v1 gw |       |        |
| 8.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok |
| 8.1.2      | API Gateway        | `routeert de lijst naar Camel Adapter`                                                      | UM-BRW1-v1    |       |        |
| 8.1.3      | Camel Adapter      | `routeert de lijst naar de Werkzoekende Bron Service`                                       |               |       |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------|
| 9.0        | Gemeentemedewerker | raadpleegt overzicht werkzoekende via menukeuze 'Overzicht geüploade werkzoekenden'         | n/a           |       |        |
| 9.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRW2-v1 gw |       |        |
| 9.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok |
| 9.1.2      | API Gateway        | `routeert de vraag naar de Werkzoekende Bron Service`                                       | UM-BRW2-v1    |       |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|-----------|-------|--------|
| 10.0       | Gemeentemedewerker | bekijkt details werkzoekende                                                                | n/a       |       |        |
| 10.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRW2-v1 gw |       |        |
| 10.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |           |       | ok/nok |
| 10.1.2     | API Gateway        | `routeert de vraag naar de Werkzoekende Bron Service`                                       | UM-BRW2-v1 |       |        |


</p></details>

<details><summary>#11 Als Bemiddelaar stel ik een vraag om op basis van kenmerken vacatures te zoeken</summary><p>

| Sequence Diagram                                                                                     |
|:-------------------------------------------------------------------------------------------------------:|
| ![userstory.png](userstories-images/userstory11.png "Als Bemiddelaar stel ik een vraag om op basis van kenmerken vactures te zoeken") |

| interactie | wie/wat                      | toelichting                                                                                                                                                                        | roept API      | input                            | output                           |
|------------|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|----------------------------------|----------------------------------|
| 11.0       | Bemiddelaar                  | kiest de menukeuze ”Vraag vacatures aan” om een vraag te kunnen stellen.                                                                                                           | n/a            |                                  |                                  |
| 11.1       | Medewerkersportaal           | stuurt de vraag naar de API Gateway.                                                                                                                                               | UM-BMW2-v1 gw  | zoekcriteria                     | ok/nok                           |
| 11.1.1     | API Gateway                  | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.`                                                                                        |                |                                  | ok/nok                           |
| 11.1.2     | API Gateway                  | `routeert de vraag naar Vacature Bemiddelaar Service`                                                                                                                              | UM-BMW2-v1     | zoekcriteria                     | ok/nok                           |
| 11.1.3     | Vacature Bemiddelaar Service | `Als je gemachtigd bent, wordt de vraag opgeslagen in de database van de Vacature Bemiddelaar Service`                                                                             |                | zoekcriteria                     | ok/nok                           |
| 11.2       | Vacature Bemiddelaar Service | zet de vraag uit via de API gateway.                                                                                                                                               | VUM-BRW1-v1 gw | zoekcriteria                     | ok/nok                           |
| 11.2.1     | API Gateway                  | `stuurt de vraag door naar VUM.`                                                                                                                                                   | VUM-BRW1-v1    |                                  |                                  |
| 11.3       | VUM                          | zet de vraag uit naar de aangesloten bronnen op basis van kriteria waaronder “bemiddelspostcode”. De vraag komt binnen op de Reverse Proxy van de Bron.                            | VUM-BRW1-v1 gw | zoekcriteria                     | ok/nok                           |
| 11.3.1     | Reverse Proxy                | `haalt de nodige informatie uit het door VUM meegeleverde Client Certificaat en zet deze informatie in de HTTP-Header. De Reverse Proxy zet het verzoek door naar de API Gateway.` | VUM-BRW1-v1 gw | zoekcriteria                     | matchende  werkzoekendeprofielen |
| 11.3.2     | API Gateway                  | `De API Gateway zet het verzoek door naar de Vacature Bron service`                                                                                                                | VUM-BRW1-v1    | zoekcriteria                     | matchende  werkzoekendeprofielen |
| 11.4       | VUM                          | Stuurt de lijst van matchende vacatures  door                                                                                                                                      | VUM-BMW3-v1 gw | matchende  werkzoekendeprofielen |                                  |
| 11.4.1     | API gateway                  | `Doorsturen van de VUM bericht`                                                                                                                                                    | VUM-BMW3 v1    | matchende  werkzoekendeprofielen |                                  |
| 11.4.2     | Vacature Bemiddelaar Service | `De matchende profielen worden opgeslagen in de database van de Vacature Bemiddelaar Service bij de oorspronkelijke vraag.`                                                        |                |                                  |                                  |



</p></details>

<details><summary>Als Bemiddelaar zoekvragen en vacatures raadplegen<br/>  
#12 Als Bemiddelaar wil ik zoekvragen raadplegen<br/>  
#13 Als Bemiddelaar wil ik zoekvraag resultaten raadplegen<br/>  
#14 Als Bemiddelaar wil ik vacatures raadplegen<br/>  
#15 Als Bemiddelaar wil ik details van een vacature raadplegen<br/>  
</summary><p>

  |Sequence Diagram|
  |:--:|
  |![userstory12-15.png](userstories-images/userstory12-15.png "Raadplegen")|

| interactie | wie/wat            | toelichting                                                                                 | roept API    | input | output                                     |
|------------|--------------------|---------------------------------------------------------------------------------------------|--------------|-------|--------------------------------------------|
| 12.0        | Bemiddelaar        | kiest de menukeuze ”Opgeslagen zoekvraag vacatures”                                     | n/a          |       |                                            |
| 12.1        | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMV3-v1 gw | oin   | vraag id, vumId, zoekvragen, vacatures |
| 12.1.1      | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |              |       | ok/nok                                     |
| 12.1.2      | API Gateway        | `routeert de vraag naar Vacature Bemiddelaar Service`                                   | UM-BMV3-v1    | oin   | vraag id, vumId, zoekvragen, vacatures |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input    | output                                 |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|----------|----------------------------------------|
| 13.0       | Bemiddelaar        | klikt op 'Bekijk vacatures'                                                                 | n/a           |          |                                        |
| 13.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMV3-v1 gw | vraag id | vraag id, vumId, zoekvragen, vacatures |
| 13.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |          | ok/nok                                 |
| 13.1.2     | API Gateway        | `routeert de vraag naar Vacature Bemiddelaar Service`                                       | UM-BMV3-v1    | vraag id | vraag id, vumId, zoekvragen, vacatures |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input       | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------------|--------|
| 14.0       | Bemiddelaar        | klikt op 'Meer detail'                                                                      | n/a           |             |        |
| 14.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BMV1-v1 gw | oin vraagId |        |
| 14.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |             | ok/nok |
| 14.1.2     | API Gateway        | `routeert de vraag naar Vacature Bemiddelaar Service`                                       | UM-BV1-v1     |             |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API    | input             | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|--------------|-------------------|--------|
| 15.0       | Bemiddelaar        | klikt op 'Vraag detail vacature aan'                                                        | n/a          |                   |        |
| 15.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BV2-v1 gw | oin vraagId vumId |        |
| 15.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |              |                   | ok/nok |
| 15.1.2     | API Gateway        | `routeert de vraag naar VUM`                                                                |              | oin vraagId vumId |        |
| 15.1.3     | Reverse proxy      | `routeert de vraag naar VUM`                                                                |              |                   |        |
| 15.1.4     | API Gateway        | `routeert de vraag naar Vacature bron service`                                              |              |                   |        |

</p></details>

<details><summary>Als Gemeentemedewerker Vacture bron vullen en raadplegen<br/>  
#16 Als Gemeentemedewerker wil ik vacatures uploaden<br/>  
#17 Als Gemeentemedewerker wil ik vacature overzicht raadplegen<br/>  
#18 Als Gemeentemedewerker wil ik details van een vacature raadplegen<br/>  
</summary><p>

  |Sequence Diagram|
  |:--:|
  |![userstory16-18.png](userstories-images/userstory16-18.png "Vacature bron")|


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------|
| 16.0       | Gemeentemedewerker | upload file via menukeuze 'Upload vacatures'                                                | n/a           |       |        |
| 16.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRV1-v1 gw |       |        |
| 16.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok |
| 16.1.2     | API Gateway        | `routeert de lijst naar Camel Adapter`                                                      | UM-BRV1-v1    |       |        |
| 16.1.3     | Camel Adapter      | `routeert de lijst naar de Vacature Bron Service`                                          |               |       |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------|
| 17.0       | Gemeentemedewerker | raadpleegt overzicht vacatures via menukeuze 'Overzicht geüploade vacatures'                | n/a           |       |        |
| 17.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRV2-v1 gw |       |        |
| 17.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok |
| 17.1.2     | API Gateway        | `routeert de vraag naar de Vacature Bron Service`                                           | UM-BRV2-v1    |       |        |


| interactie | wie/wat            | toelichting                                                                                 | roept API     | input | output |
|------------|--------------------|---------------------------------------------------------------------------------------------|---------------|-------|--------|
| 18.0       | Gemeentemedewerker | bekijkt details vacature                                                                    | n/a           |       |        |
| 18.1       | Medewerkersportaal | stuurt de vraag naar de API Gateway.                                                        | UM-BRV2-v1 gw |       |        |
| 18.1.1     | API Gateway        | `controleert bij de AIM Server Keycloak of je gemachtigd bent om dit verzoek in te dienen.` |               |       | ok/nok |
| 18.1.2     | API Gateway        | `routeert de vraag naar de Vacature Bron Service`                                           | UM-BVW2-v1    |       |        |

### schermafeeldingen van upload werkzoekende en upload vacatures en overzicht geuploade werkzoekende en geuploade vacatures.

![screenshot1.png](../Screenshots/screenshot1.png)

![screenshot2.png](../Screenshots/screenshot2.png)

![screenshot3.png](../Screenshots/screenshot3.png)

![screenshot4.png](../Screenshots/screenshot4.png)

</p></details>