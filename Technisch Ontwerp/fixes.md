# User stories

versie 0.8

_Versionering_  

| versie | datum          | opmerking                             |
|--------|----------------|---------------------------------------|
| 0.1    | september 2022 | Initiele opzet                        |
| 0.8    | oktober 2022   | Werkzoekende en vacature user stories |


## 1.1 Doel

Dit document bevat verwijzingen naar (gearchiveerde) issues op het [Trello Bord](https://trello.com/b/AVKsUzC5/um-pilot)

- CORS headers die niet gezet worden: // disable CORS and CSRF TODO: in production, check this.  
- .env maken voor de environment variabelen.  
- UM URL's aanpassen naar variabelen  
- Als een gebruiker van een docker image wil ik weten op basis van welke versie van de code dit image is gebouwd  