# Componenten UM omgeving

versie 1.0

_Versionering_  

| versie | datum         | opmerking                                     |
|--------|---------------|-----------------------------------------------|
| 0.1    | augustus 2022 | Initiele opzet                                |
| 0.5    | oktober 2022  | Componenten, services en configuratie         |
| 0.6    | januari 2023  | Bijgewerkt nav meerdere gewijzigde properties |
| 1.0    | maart 2023    | Bijgewerkt naar huidige versie UM             |


## Componenten UM

### 1.1 Doel
Dit document beschrijft de componenten van de UM omgeving met de belangrijkste kenmerken en configuratiefiles. 
Tevens bevat dit document een lijst van API's (web services).

### 1.2 Overzicht van de componenten

|*UM architectuur*|
|:--:|
|![UMarchitectuur.svg](/Technisch%20Ontwerp/architectuur-images/UMarchitectuur.svg "UMarchitectuur.svg")|

<details><summary>Toelichting UM architectuur per component</summary><p>

| Component                | Korte omschrijving                                                                                                                            | Technology     |
|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| VUM                      | Project Verbeteren Uitwisseling Matchingsgegevens (VUM).  Uitwisselen van werkzoekenden- en vacaturebestand over regio- en organisatiegrenzen | <buiten scope> |
| bestandsconverter        | Biedt functionaliteit om excel files te importeren om UM van gegevens te voorzien                                                             |                |
| adapter                  | Dit betreft de camel-adapter die het mogelijk maakt excel files te importeren om UM van gegevens te voorzien.                                 | Apache Camel   |
| vacatures-bron           | Bevat vacatures                                                                                                                               | Java           |
| vacatures-bemiddelaar    | Vraagt vacatures op bij VUM en koppelt binnenkomende antwoorden aan deze zoekvraag                                                            | Java           |
| werkzoekendeProfielen-bemiddelaar | Vraagt werkzoekende profielen op bij VUM en koppelt binnenkomende antwoorden aan deze zoekvraag                                               | Java           |
| werkzoekendeProfielen-bron        | Bevat werkzoekende profielen                                                                                                                  | Java           |
| API gateway              | Apigateway. Verantwoordelijk voor routering en API security                                                                                   | Kong           |
| keycloak                 | Authenticatie/Autorisatieserver (Identity and Access Management). <br> Bevat de gebruikersprofielen (gebruikersrollen en OIN)                 | Keycloak       |
| elastic-search           | T.b.v. verzamelen logging                                                                                                                     | Elastic search |
| vng-webapp               | Medewerkersportaal                                                                                                                  | Javascript     |

| Component                | Korte omschrijving                                                                                            | Technology     |
|--------------------------|---------------------------------------------------------------------------------------------------------------|----------------|
| nginx                    | zet de TLS communicatie naar de hostname over naar HTTP calls op het interne IP + port-nummer                 | Nginx          |

</p></details>

<details><summary>Toelichting UM architectuur per service</summary><p>

| API ID              | Geboden functionaliteit                                              | methode   | van>naar                                                         | Path in Kong (Kong route)                                                  | Target url                                                                                                                                                                        |
|---------------------|----------------------------------------------------------------------|-----------|------------------------------------------------------------------|----------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ------------------- | **VUM Bemiddelaar WerkzoekendeProfielen***                           | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| VUM-BMW1-v1         | Zoekopdracht voor MatchingProfielen werkzoekende                     | POST      | werkzoekendeProfielen-bemiddelaar><br>VUM                        | /vumprofielvraag/werkzoekendeProfielen/matches<br>(vum-werkzoekende-vraag) | /v1/werkzoekendeProfielen/matches                                                                                                                                                 |
| VUM-BMW2-v1         | Vraag detailprofielen op bij opgegeven vumID                         | GET       | werkzoekendeProfielen-bemiddelaar><br>VUM                        | /vumprofielvraag/werkzoekendeProfielen/{vumID}<br>(vum-werkzoekende-vraag) | /v1/werkzoekendeProfielen/{vumID}                                                                                                                                                 |
| VUM-BMW3-v1         | Aanleveren werkzoekenden - Asynchroon antwoord op zoekopdracht       | POST      | VUM><br>werkzoekendeProfielen-bemiddelaar                        | /aanvraagwerkzoekende/callback<br>(werkzoekende-bemiddelaar)               |                                                                                                                                                                                   |
| ------------------- | **VUM Bemiddelaar Vacatures***                                       | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| VUM-BMV1-v1         | Zoekopdracht voor Vacatures                                          | POST      | vacatures-bemiddelaar><br>VUM                                    | /vumvacaturevraag/vacatures/matches<br>(vum-vacature-vraag)                | /v1/vacatures/matches                                                                                                                                                             |
| VUM-BMV2-v1         | Vraag volledige vacature op bij opgegeven vumID                      | GET       | vacatures-bemiddelaar>VUM                                        | /vumvacaturevraag/vacatures/{vumID}<br>(vum-vacature-vraag)                | /v1/vacatures/{vumID}                                                                                                                                                             |
| VUM-BMV3-v1         | Aanleveren vacatures - Asynchroon antwoord op zoekopdracht           | POST      | VUM><br>vacatures-bemiddelaar>                                   | /vactures/callback<br>(vacature-bemiddelaar)                               |                                                                                                                                                                                   |
| ------------------- | **VUM Bron WerkzoekendeProfielen***                                  | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| VUM-BRW1-v1         | Zoekopdracht voor MatchingProfielen Werkzoekende                     | POST      | VUM><br>werkzoekendeProfielen-bron                               | /werkzoekendeProfielen/matches(vum-backend-werkzoekende-bron)              | /werkzoekendeProfielen/matches                                                                                                                                                    |
| VUM-BRW2-v1         | Vraag detailprofielen op bij opgegeven idWerkzoekende                | GET       | VUM><br>werkzoekendeProfielen-bron                               | /werkzoekendeProfielen/{idWerkzoekende}<br>(vum-backend-werkzoekende-bron) | /werkzoekendeProfielen/{idWerkzoekende}                                                                                                                                           |
| ------------------- | **VUM Bron Vacatures***                                              | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| VUM-BRV1-v1         | Zoekopdracht voor Vacatures                                          | POST      | VUM><br>vacature-bron                                            | /vacatures/matches<br>(vum-backend-vacature-bron)                          | /vacatures/matches                                                                                                                                                                |
| VUM-BRV2-v1         | Vraag volledige vacature op bij opgegeven idVacature                 | GET       | VUM><br>vacature-bron                                            | /vacatures/{idVacature}<br>(vum-backend-vacature-bron)                     | /vacatures/{idVacature}                                                                                                                                                           |
| ------------------- | **Ondersteunende services**                                          | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| UM-SEC1-v1          | Ten behoeve inloggen                                                 | GET       | Medewerkersportaal>Keycloak                                      |                                                                            | /auth/realms/{realm id}/protocol/openid-connect/token                                                                                                                             |
| UM-SEC2-v1          | Ophalen access token                                                 | POST      | meerdere UM componenten >Keycloak                                |                                                                            | /auth/realms/{realm id}/protocol/openid-connect/auth?response_type=code& client_id={}& state={}& redirect_uri={}& scope={}& code_challenge={}& code_challenge_method={}& nonce={} |
| ------------------- | **WerkzoekendeProfielen bemiddelaar services**                       | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| UM-BMW1-v1          | AanvraagWerkzoekende                                                 | GET       | Medewerkersportaal>werkzoekendeProfielen-bemiddelaar             | /aanvraagwerkzoekende/lijst/{oin}                                          |                                                                                                                                                                                   |
| UM-BMW2-v1          | requestVacatures    MatchesRequestGemeente                           | POST      | Medewerkersportaal><br/>werkzoekendeProfielen-bemiddelaar        | /aanvraagwerkzoekende/{oin}                                                |                                                                                                                                                                                   |
| UM-BMW3-v1          | AanvraagWerkzoekende                                                 | GET       | Medewerkersportaal><br/>werkzoekendeProfielen-bemiddelaar        | /aanvraagwerkzoekende/{oin}/{vraagId}                                      |                                                                                                                                                                                   |
| UM-BMW4-v1          | getDetailVacature                                                    | GET       | Medewerkersportaal>werkzoekendeProfielen-bemiddelaar             | /aanvraagwerkzoekende/detail/{oin}/{vraagId}/{vumId}                       |                                                                                                                                                                                   |
| ------------------- | **Vacatures bemiddelaar services**                                   | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| UM-BMV1-v1          | getVraagId AanvraagVacature                                          | GET       | Medewerkersportaal><br/>vacature-bemiddelaar                     | /aanvraagvacature/{oin}/{id}                                               |                                                                                                                                                                                   |
| UM-BMV2-v1          | getDetailVacature                                                    | GET       | Medewerkersportaal><br/>vacature-bemiddelaar                     | /aanvraagvacature/detail/{oin}/{vraagId}/{vumId}                           |                                                                                                                                                                                   |
| UM-BMV3-v1          | getAllVraagId                                                        | GET       | Medewerkersportaal><br/>vacature-bemiddelaar                     | /aanvraagvacature/lijst/{oin}                                              |                                                                                                                                                                                   |
| UM-BMV4-v1          | requestVacatures                                                     | POST      | Medewerkersportaal><br/>vacature-bemiddelaar                     | /aanvraagvacature/{oin}                                                    |                                                                                                                                                                                   |
| ------------------- | **Vacatures bron service**                                           | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| UM-BRV1-v1          | Vacatures beschikbaar stellen                                        | POST      | Medewerkersportaal><br/>vacature-bron <br/>via Camel             | /camel/upload/report/                                                      |                                                                                                                                                                                   |
| UM-BRV2-v1          | Lijst van alle vacatures                                             | GET       | Medewerkersportaal><br/>vacature-bron                            | /vacature/lijst/{oin}                                                      |                                                                                                                                                                                   |
| ------------------- | **WerkzoekendeProfielen bron services**                              | --------- | ------------------------------                                   | -----------------------------------                                        | ---------------------------------------------                                                                                                                                     |
| UM-BRW1-v1          | Werkzoekende beschikbaar stellen (Upload werkzoekendeProfielen bron) | POST      | Medewerkersportaal><br/>werkzoekendeProfielen-bron<br/>via Camel | /camel/upload/report/                                                      | camel-adapter:8080/camel/upload/repor                                                                                                                                             |
| UM-BRW2-v1          | Lijst van alle werkzoekendeProfielen                                 | GET       | Medewerkersportaal><br/>werkzoekendeProfielen-bron               | /werkzoekende/lijst/{oin}                                                  |                                                                                                                                                                                   |

* De specificaties van de door VUM uitgegeven API's zijn hier terug te vinden: [VUM koppelvlak specificaties](https://gitlab.com/inlichtingenbureau/vum/koppelvlak/)

API ID opgebouwd als volgt: [eigenaar API]-[kenmerk]  
Indien versie API bekend: [eigenaar API]-[kenmerk]-v[API-versie]  


</p></details>

### 1.3 Security en Compliance

#### OpenID Connect (OIDC)
OpenID Connect stelt applicaties (clients) in staat om de identiteit van de eindgebruiker te verifiëren op basis van de authenticatie door een autorisatieserver, en om basisprofielinformatie over de eindgebruiker (gebruikersprofiel) te verkrijgen.  
In de UM applicatie vervult Keycloack de rol van autorisatie server, waarmee verschillende componenten van UM mee verbinden. 
Zie voor de configuratie details de paragraaf 'Detaillering per component hieronder'.

#### SSL/MTLS
Zorgt voor beveiligde verbindingen, binnen UM toegpast op verbindingen die extern gelegd worden, bijvoorbeeld met VUM.

### 1.4 Overzicht Docker Images

Het gebruik van Docker Containers biedt een optie om de UM applicatie te laten draaien.  
De verschillende UM componenten zijn verdeeld over verschillende Docker Containers. Aan de basis van elke Docker Container staat een Docker Image.  
Om de verschillende containers in samenhang te kunnen draaien, wordt gebruik gemaakt van Docker Compose.
Docker Compose gebruikt hierbij de 'instructies' uit configuratie file [docker compose file][101], welke zich bevindt in het [um-compose git repo][100].  

Hieronder in de tabel het overzicht van de verschillende Docker Images, hun toegangspoorten en configuratie files, zoals geconfigureerd in de docker compose file:  

| Componenten              | Docker Image                                  | poorten*                     | configuratie files                                                                                                                                                                     |
|--------------------------|-----------------------------------------------|------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| adapter                  | vngrci/adapter                                |                              |                                                                                                                                                                                        |
| vacatures-bron           | vngrci/vacatures-bron                         |                              |                                                                                                                                                                                        |
| vacatures-bemiddelaar    | vngrci/vacatures-bemiddelaar                  |                              |                                                                                                                                                                                        |
| werkzoekende-bemiddelaar | vngrci/werkzoekendenprofielen-bemiddelaar     |                              |                                                                                                                                                                                        |
| werkzoekende-bron        | vngrci/werkzoekendenprofielen-bron            |                              |                                                                                                                                                                                        |
| gateway                  | vngrci/gateway                                | 8081:8080 <br> 8483:8081 SSL | ../gateway/conf/kong/kong.compose.conf:/etc/kong/kong.conf <br>../gateway/conf/gateway/kong.compose.yml:/usr/local/kong/declarative/kong.yml<br>../gateway/conf/certs:/usr/local/certs |
| keycloak                 | jboss/keycloak                                | 8083:8080                    | ../keycloak/conf/realm-export.json:/opt/jboss/keycloak/imports/realm-export.json<br>../keycloak/resources/theme:/opt/jboss/keycloak/themes/customlogin/                                |
| elastic-search           | docker.elastic.co/elasticsearch/elasticsearch | 8084:9200                    |                                                                                                                                                                                        |
| vng-webapp               | vngrci/web-applicatie                         | 8080:8080                    |                                                                                                                                                                                        |

*poorten - dit zijn de poort mappings geconfigureerd door docker. HOST PORT :INTERNE COMPONENT PORT. De gateway is buiten de gateway zelf bereikbaar op poort 8081.


### 1.5 Detaillering per component

#### 1.5.1 vng-webapp

<details><summary>Aanvullende details properties</summary><p>

Locatie: [environment properties][302]

| Variabel                             | Beschrijving                                                                                              | Voorbeeld                                                                                                                 |
|--------------------------------------|-----------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| GATEWAY_URL                          | Url Api Gateway (Kong)                                                                                    |                                                                                                                           |
| KIBANA_URL                           | Url Kibana                                                                                                |                                                                                                                           |
| STRICT_DISCOVERY_DOCUMENT_VALIDATION | Toepassen strikte validatie Oauth discovery document. Afhankelijk van Identity Server of dit mogelijk is. | true (Keycloak) <br> false (Azure AD)                                                                                     |
| CLIENT_ID                            | Bij gebruik Keycloak als Identity server het id van de realm                                              | {{realm id}} (Keycloak)   {{tenantID}} (Azure AD)                                                                         |
| SCOPE                                | Scope bij Identity Provider                                                                               | "openid profile email offline_access" (Keycloak) <br>   api://{{tenantID}}/{{scope id}} (Azure AD)                             |
| REQUIRE_HTTPS                        | Bepaalt of HTTPS vereist is voor connectie met Identity Server.                                           | true                                                                                                                      |
| FRONT_END_JWT_ISSUER_URI             | JWT Issuer URI                                                                                            | {{Identity Server URL}}/auth/realms/ (Keycloak)  <br> "https://login.microsoftonline.com/ {{tenant id}} /v2.0" (Azure AD) |

</p></details>

#### 1.5.2 vacatures-bemiddelaar

<details><summary>Aanvullende details voor application properties:</summary><p>

Locatie: [application properties][402]

| Variabel                     | Beschrijving                                                    |
|------------------------------|-----------------------------------------------------------------|
| BMV_SERVER_PORT              |                                                                 |
| BMV_SERVER_ADDRESS           |                                                                 |
| BMV_LOGGING_LEVEL            |                                                                 |
| BMV_LOGGING_FILE_PATH        |                                                                 |
| BMV_LOGGING_FILE_NAME        |                                                                 |
| BMV_JWT_ISSUER_URI           |                                                                 |
| BMV_CONSOLE_ENABLED          | Console van interne H2 database. False bij gebruiker andere DB. |
| BMV_DATASOURCE_URL           |                                                                 |
| BMV_DATASOURCE_DRIVER        |                                                                 |
| BMV_DATASOURCE_USERNAME      |                                                                 |
| BMV_DATASOURCE_PASSWORD      |                                                                 |
| BMV_DATABASE_PLATFORM        |                                                                 |
| BMV_HIBERNATE_DDL            |                                                                 |
| BMV_DAYS_TO_EXPIRE           |                                                                 |
| BMV_MAX_DETAIL_PROFILES      |                                                                 |
| BMV_CALLBACK_URL_BEMIDDELAAR |                                                                 |
| BMV_VUM_PROVIDER_OIN         | OIN van de VUM provider                                         |
| BMV_VUM_URL_MATCHES          |                                                                 |
| BMV_VUM_ID_URL               |                                                                 |
| BMV_BEMIDDELAAR_PROVIDER_OIN | OIN van de bemiddelaar provider. Optioneel.                     |

De OIN van de bemiddelaar provider is verplicht, indien deze afwijkt van de de OIN van de bemiddelaar welke is opgeslagen als OIN in het gebruikersprofiel.
</p></details>


#### 1.5.3 vacatures-bron

<details><summary>Aanvullende details voor application properties:</summary><p>

Locatie: [application properties][452]

| Variabel                | Beschrijving            |
|-------------------------|-------------------------|
| BRV_SERVER_PORT         |                         |
| BRV_SERVER_ADDRESS      |                         |
| BRV_LOGGING_LEVEL       |                         |
| BRV_LOGGING_FILE_PATH   |                         |
| BRV_LOGGING_FILE_NAME   |                         |
| BRV_JWT_ISSUER_URI      |                         |
| BRV_CONSOLE_ENABLED     |                         |
| BRV_DATASOURCE_URL      |                         |
| BRV_DATASOURCE_DRIVER   |                         |
| BRV_DATASOURCE_USERNAME |                         |
| BRV_DATASOURCE_PASSWORD |                         |
| BRV_DATABASE_PLATFORM   |                         |
| BRV_HIBERNATE_DDL       |                         |
| BRV_ELASTICSEARCH_URL   |                         |
| BRV_MAX_AMOUNT_RESPONSE |                         |
| BRV_VUM_PROVIDER_OIN    | OIN van de VUM provider |

</p></details>


#### 1.5.4 werkzoekendeProfielen-bemiddelaar

<details><summary>Aanvullende details voor application properties:</summary><p>

Locatie: [application properties][502] 

| Variabel                     | Beschrijving                                |
|------------------------------|---------------------------------------------|
| BMW_SERVER_PORT              |                                             |
| BMW_SERVER_ADDRESS           |                                             |
| BMW_LOGGING_LEVEL            |                                             |
| BMW_LOGGING_FILE_PATH        |                                             |
| BMW_LOGGING_FILE_NAME        |                                             |
| BMW_JWT_ISSUER_URI           |                                             |
| BMW_CONSOLE_ENABLED          |                                             |
| BMW_DATASOURCE_URL           |                                             |
| BMW_DATASOURCE_DRIVER        |                                             |
| BMW_DATASOURCE_USERNAME      |                                             |
| BMW_DATASOURCE_PASSWORD      |                                             |
| BMW_DATABASE_PLATFORM        |                                             |
| BMW_HIBERNATE_DDL            |                                             |
| BMW_DAYS_TO_EXPIRE           |                                             |
| BMW_MAX_DETAIL_PROFILES      |                                             |
| BMW_CALLBACK_URL_BEMIDDELAAR |                                             |
| BMW_VUM_PROVIDER_OIN         | OIN van de VUM provider                     |
| BMW_VUM_URL_MATCHES          |                                             |
| BMW_VUM_ID_URL               |                                             |
| BMW_BEMIDDELAAR_PROVIDER_OIN | OIN van de bemiddelaar provider. Optioneel. |

De OIN van de bemiddelaar provider is verplicht, indien deze afwijkt van de de OIN van de bemiddelaar welke is opgeslagen als OIN in het gebruikersprofiel.
</p></details>




#### 1.5.5 werkzoekendeProfielen-bron

<details><summary>Aanvullende details voor application properties:</summary><p>

Locatie: [application properties][552]

| Variabel                | Beschrijving |
|-------------------------|--------------|
| BRW_SERVER_PORT         |              |
| BRW_SERVER_ADDRESS      |              |
| BRW_LOGGING_LEVEL       |              |
| BRW_LOGGING_FILE_PATH   |              |
| BRW_LOGGING_FILE_NAME   |              |
| BMW_JWT_ISSUER_URI      |              |
| BRW_CONSOLE_ENABLED     |              |
| BRW_DATASOURCE_URL      |              |
| BRW_DATASOURCE_DRIVER   |              |
| BRW_DATASOURCE_USERNAME |              |
| BRW_DATASOURCE_PASSWORD |              |
| BRW_DATABASE_PLATFORM   |              |
| BRW_HIBERNATE_DDL       |              |
| BRW_LIQUIBASE_ENABLED   |              |
| BRW_ELASTICSEARCH_URL   |              |
| BRW_MAX_AMOUNT_RESPONSE |              |
| BRW_VUM_PROVIDER_OIN    | OIN van de VUM provider                     |

</p></details>

#### 1.5.6 Kong Api Gateway
De Api Gateway gerealiseerd met Kong API Gateway, versie 2.6 in DBLess mode. 
De configuratie van de Gateway is verdeeld over verschillende files:
* [Kong.compose.conf][202]: oa poorten, logging en plugins.
* [Kong.compose.yml][203]: inrichting Api's.

Deze files worden gebruikt door:
* de [dockerfile][201] bij het maken van een Kong Docker image.
* de [docker compose file][101], die gebruik maakt van het aangemaakte Kong Docker image, en de hierboven genoemde files in het image overschrijft wanneer de container start (docker compose up), tezamen met de SSL certificaten.

De volgende Kong plugins worden gebruikt:
* [CORS][2001]
  * Alleen bedoeld voor development(?).
  * Geconfigureerd in kong.compose.yml.
* [OpenID Connect][2002]
  * Deze community plugin werkt op het moment van schrijven niet samen met Kong API Gateway versie 3.0. 
  * Deze wordt geinstalleerd bij het aanmaken van het image, zie de eerder genoemde dockerfile.
  * Geconfigureerd in zowel kong.compose.yml als kong.compose.conf.

<details><summary>Aanvullende details kong.compose.conf</summary><p>

```
database = off
declarative_config = /usr/local/kong/declarative/kong.yml
proxy_access_log = /dev/stdout
admin_access_log = /dev/stdout
proxy_error_log = /dev/stderr
admin_error_log = /dev/stderr
proxy_listen = 0.0.0.0:8080, 0.0.0.0:8081 ssl
admin_listen = 0.0.0.0:8082, 0.0.0.0:8083 ssl
plugins = oidc
```
</p></details>

<details><summary>Aanvullende details kong.compose.yml</summary><p>

In deze file zijn geconfigureerd: 
* upstream [services](#aangeboden-services)
    * per service een route, die bepaalt hoe downstream applicaties Kong moet aanroepen
    * per service optionele configuratie ten behoeve van Open ID Connect (OIDC) en SSL certficaten.
* plugins geldig voor alle services

De onderstaande OIDC configuratie wordt toegepast op services en moet aansluiten op de Keycloak configuratie.
* De client_id, client_secret, realm worden geconfigureerd in Keycloak en moeten in de onderstaande configuratie worden doorgevoerd.

```
  services:
    - name: aName
      url: http://example
      plugins:
      - name: oidc
        config: 
          client_secret: ofsdfsdfGevwhohahaMD5TpZzMn
          client_id: backend-clientid
          bearer_only: 'yes'
          realm: vng-realm
          introspection_endpoint: http://keycloak:8083/auth/realms/vng-realm/protocol/openid-connect/token/introspect 
          discovery: http://keycloak:8083/auth/realms/master/.well-known/openid-configuration
```
</p></details>

<details><summary>Aanvullende details docker compose file</summary><p>

Locatie: [docker compose file][101]

Extra_hosts gezet in de docker compose file
extra_hosts,bv:
```
      - "<um-host>.nl:192.168.104.50"
      - "<vum-host>:192.168.104.50"
      - "<vum.host>:192.168.104.50"
```
</p></details>

#### 1.5.7 adapter

<details><summary>Aanvullende details voor application properties:</summary><p>

Locatie: [application properties][602]

| Variabel                       | Beschrijving          |
|--------------------------------|-----------------------|
| CA_SERVER_PORT                 |                       |                     
| CA_SERVER_ADDRESS              |                       |                     
| CA_BACKEND_URL                 |                       |                    

</p></details>


#### 1.5.8 Keycloak

De volgende clients moeten worden geconfigureerd ten behoeve van OpenID Connect. Deze configuratie is opgenomen in de Realm file(s) en gebruikt in de [docker compose file][101].
Merk op dat OpenID Connect Clients (conform de standaarden) geconfigureerd wordt ten behoeve applicaties, en geen eindgebruikers (user) representeren.

**'frontend-client'** 
* Client opgezet in Keycloak. Wordt gebruikt door medewerkersportaal ten behoeven van OpenID Connect.
* Huidige configuratie in Keycloak admin console:
```
  Acccess Type 'public' waardoor geen Client Secret aanwezig is. Inlog protocol kan starten zonder Client Secret.
  Root URL ''
  Valid Redirect URIs 'http://localhost:8080/*', 'http://localhost:8080'
  Base URL 'http://localhost:8080'
  Web Origins 'http://localhost:8080'
```

**'backend-client'** 
* Client opgezet in Keycloak t.b.v. Kong API Gateway / andere APP's die geen eindgebruikers login proces initieren (Client Secret is wel verplicht).
* Het Client secret is onderdeel van de Realm file, zodat dit secret niet bij elke opbouw van een Keycloak Docker container opnieuw random gegenereerd wordt. Voor niet-ontwikkelomgevingen moet dit secret uiteraard niet geopenbaard worden.
* Huidige configuratie in Keycloak admin console:
```
  Acccess Type 'bearer only' waardoor een client secret in de communicatie gebruikt moet worden. Zie tab 'Credential' om er één te genereren. Kan niet worden gebruikt worden om inlog protocol te initieren.
  Root URL n/a
  Valid Redirect URIs n/a
  Base URL n/a
  Web Origins n/a
```
</p></details>

<details><summary>Aanvullende details docker compose file</summary><p>

Locatie: [docker compose file][251]

Environment variables gezet in de docker compose file van Keycloak
```
    image: jboss/keycloak:${VERSION}
    container_name: ${KEYCLOAK_CONTAINER_NAME}
    volumes:
      - ${KEYCLOAK_CONFIG_REALM_DIR}:/opt/jboss/keycloak/realm-conf/
      - ${KEYCLOAK_CONFIG_THEME}:/opt/jboss/keycloak/themes/customlogin/
    environment:
      PROXY_ADDRESS_FORWARDING: ${PROXY_ADDRESS_FORWARDING}
      KEYCLOAK_FRONTEND_URL: ${KEYCLOAK_FRONTEND_URL}
      KEYCLOAK_LOGLEVEL: ${KEYCLOAK_LOGLEVEL}
      KEYCLOAK_USER: ${KEYCLOAK_USER}
      KEYCLOAK_PASSWORD: ${KEYCLOAK_PASSWORD}
```

waarbij de variabele uit ```${variabele:default waarde}``` zijn waarde krijgt de gebruikte properties file bij het opstarten van Keycloak.

Merk het volgende op:

| variabele                 | opmerking                                                                                                                                                          |
|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| KEYCLOAK_CONFIG_REALM_DIR | Files in de aangegeven directory, waarvan de bestandsnaam eindigt op *realm.json, worden als realm configuratie files en ingelezen bij het opgstarten van Keycloak |
| KEYCLOAK_FRONTEND_URL     | gevuld met een waarde die overeenkomst met het eerste gedeelte uit de [JWT_ISSUER_URI][102] gebruikt in het um-compose project                                     |
| PROXY_ADDRESS_FORWARDING  | moet op true staan indien voor Keycloak een reversed proxy is geplaatst                                                                                            |



</p></details>



#### 1.5.9 elasticsearch



[100]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose
[101]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/blob/develop/docker-compose.yml
[102]:  https://gitlab.com/vng-realisatie/um-pilot/um-compose/-/blob/develop/.env

[200]:  https://gitlab.com/vng-realisatie/um-pilot/gateway
[201]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/Dockerfile
[202]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/conf/kong/kong.compose.conf
[203]:  https://gitlab.com/vng-realisatie/um-pilot/gateway/-/blob/develop/conf/gateway/kong.compose.yml

[2001]: https://docs.konghq.com/hub/kong-inc/cors/
[2002]: https://github.com/nokia/kong-oidc

[250]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak
[251]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak/-/blob/develop/docker/docker-compose.yml
[252]:  https://gitlab.com/vng-realisatie/um-pilot/keycloak/-/blob/develop/docker/.env

[300]:  https://gitlab.com/vng-realisatie/um-pilot/web-applicatie
[302]:  https://gitlab.com/vng-realisatie/um-pilot/web-applicatie/-/blob/develop/src/environments/environment.ts

[400]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bemiddelaar
[402]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bemiddelaar/-/blob/develop/src/main/resources/application.properties

[450]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bron
[452]:  https://gitlab.com/vng-realisatie/um-pilot/vacatures-bron/-/blob/develop/src/main/resources/application.properties

[500]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bemiddelaar
[502]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bemiddelaar/-/blob/develop/src/main/resources/application.properties

[550]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bron
[552]:  https://gitlab.com/vng-realisatie/um-pilot/werkzoekendenprofielen-bron/-/blob/develop/src/main/resources/application.properties

[600]:  https://gitlab.com/vng-realisatie/um-pilot/camel-adapter
[602]:  https://gitlab.com/vng-realisatie/um-pilot/camel-adapter/-/blob/develop/src/main/resources/application.properties

[700]:  https://gitlab.com/vng-realisatie/um-pilot/elk

[800]:  https://gitlab.com/vng-realisatie/um-pilot/stub-vacatures

[850]:  https://gitlab.com/vng-realisatie/um-pilot/stub-werkzoekendeprofielen

