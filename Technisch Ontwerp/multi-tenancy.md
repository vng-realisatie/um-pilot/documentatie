# User stories

versie 0.8

_Versionering_  

| versie | datum          | opmerking                             |
|--------|----------------|---------------------------------------|
| 0.1    | september 2022 | Initiele opzet                        |
| 0.8    | oktober 2022   | Werkzoekende en vacature user stories |


## 1.1 Doel

Dit document bevat verwijzingen naar (gearchiveerde) issues op het [Trello Bord](https://trello.com/b/AVKsUzC5/um-pilot) die betrekking hebben op multi-tenancy

- inlogscherm achtergrond aanpassen
